package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.ClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ClienteControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ClienteDTO clienteDTO;

    private ClienteEntity cliente;

    private ObjectMapper mapper;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        clienteDTO = new ClienteDTO(
                "NomeTeste",
                "45781874250",
                "dummy@email.com",
                "44778899");

        cliente = clienteRepository.save(
                new ClienteEntity(
                    clienteDTO.getNome(),
                    clienteDTO.getCpf(),
                    clienteDTO.getDataNascimento()
                )
        );

        mapper = new ObjectMapper();
    }

    @Test
    public void deveRetornar200QuandoCliente() throws Exception {

        URI uri = new URI("/api/cliente/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarUmClientePorEmail() throws Exception {

        URI uri = new URI("/api/cliente/buscar/por-cpf/45781874250");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("NomeTeste")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.cpf")
                                .value("45781874250")
                );
    }

    @Test
    public void salvarERetornarUmCliente() throws Exception {

        URI uri = new URI("/api/cliente/novo");

        clienteDTO = new ClienteDTO(
                "NomeTeste2",
                "99988877744",
                "dummy2@email.com",
                "44798899");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(clienteDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("NomeTeste2")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.cpf")
                                .value("99988877744")
                );
    }

    @Test
    public void retornarClienteAtualizado() throws Exception {

        URI uri = new URI("/api/cliente/editar/1");

        clienteDTO.setNome("OutroNome");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(clienteDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .put(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("OutroNome")
                );
    }
}
