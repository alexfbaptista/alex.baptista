package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacoService extends ServiceAbstract<EspacoRepository, EspacoEntity, Integer> {

    public EspacoEntity buscarPorNome(String nome) {
        return repository.findByNome(nome);
    }

    public EspacoEntity buscarPorQuantidadeDePessoas(int quantidade) {
        return repository.findByQtdPessoas(quantidade);
    }

    public List<EspacoEntity> buscarPorQuantidadeDePessoasMaiorOuIgual(int quantidade) {
        return repository.findByQtdPessoasGreaterThanEqual(quantidade);
    }
}
