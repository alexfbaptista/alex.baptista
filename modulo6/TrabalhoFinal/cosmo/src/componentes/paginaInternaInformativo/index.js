import React from 'react';
import BotaoUi from '../../componentes/botaoUi';

const Informativo = () =>
    <React.Fragment>
        <div className='row interna-informativo'>
          <div className='col col-sm-10'>
            <h3>
              essa loja oferece até <span className='letra-salmao'>30% de desconto + frete único</span> nas compras de sacolinha
            </h3>
          </div>
          <div className='col col-sm-2 interna-informativo-container-botao'>
            <BotaoUi nome={ 'ver mais' } classe={ 'botao-informativo-interno' } />
          </div>
        </div>
    </React.Fragment>

export default Informativo;