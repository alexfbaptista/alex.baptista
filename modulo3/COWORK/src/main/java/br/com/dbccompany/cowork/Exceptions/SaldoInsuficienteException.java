package br.com.dbccompany.cowork.Exceptions;

public class SaldoInsuficienteException extends Exception {

    private String mensagem;

    public SaldoInsuficienteException(String mensagem) {
        super("Erro: saldo insuficiente para realizar entrada no cowork! saldo atual: " + mensagem);
        this.mensagem = mensagem;
    }
}
