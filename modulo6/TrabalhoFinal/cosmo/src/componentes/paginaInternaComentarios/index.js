import React from 'react';
import { Input, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const { Search } = Input;

const Comentarios = ({ metodo, comentarios }) => (
    <React.Fragment>
          <div className='col col-sm-6 descricao-detalhes-comentarios-container'>
            <div className='row'>
              <Search
                placeholder="Deixe seu comentário"
                allowClear
                enterButton="Enviar"
                size="large"
                onSearch={ metodo }
                maxlength="280"
              />
            </div>
            <div className='row'>
            {
              comentarios && comentarios.map( ( e, index ) => 
                <section>
                    <p key={ index } className='col col-sm-12 container-comentarios-interno'>
                      <Avatar icon={<UserOutlined />} />
                      <span className='paragrafo-comentarios-texto'>{ `${ e.usuario }:  ${e.comentario}` }</span>
                    </p>
                </section> )
            }
            </div>
          </div>
    </React.Fragment >
)

export default Comentarios;