package br.com.dbccompany.cowork.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class TipoContatoEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @OneToMany (mappedBy = "tipoContato", fetch = FetchType.EAGER)
    private Set<ContatoEntity> contatos;

    public TipoContatoEntity() {
    }

    public TipoContatoEntity(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(Set<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
