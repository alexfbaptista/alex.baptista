package br.com.dbccompany.cowork.Entity;

import br.com.dbccompany.cowork.Entity.Enum.TipoPagamentoEnum;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PagamentoEntity.class)
public class PagamentoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientePacoteEntity clientePacote;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoEntity() {
    }

    public PagamentoEntity(ClientePacoteEntity clientePacote, TipoPagamentoEnum tipoPagamento) {
        this.clientePacote = clientePacote;
        this.tipoPagamento = tipoPagamento;
    }

    public PagamentoEntity(ContratacaoEntity contratacao, TipoPagamentoEnum tipoPagamento) {
        this.contratacao = contratacao;
        this.tipoPagamento = tipoPagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
