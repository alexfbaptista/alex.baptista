package br.com.dbccompany.cowork.Service;


import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.PacoteDTO;
import br.com.dbccompany.cowork.DTO.DadosDoPacotePorEspacoDTO;
import br.com.dbccompany.cowork.Entity.ClientePacoteEntity;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.PacoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PacoteService {

    @Autowired
    private ClientePacoteService clientePacoteService;

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Transactional(rollbackFor = Exception.class)
    public Double salvar(PacoteDTO pacoteDTO) throws Exception {

        Double valorTotal = 0.0;

        PacoteEntity pacote = pacoteRepository.save(new PacoteEntity(
                valorTotal
        ));

        for (DadosDoPacotePorEspacoDTO subObjeto : pacoteDTO.getPacoteSubparteDTOS()) {
            valorTotal = salvaEspacoPacoteRetornaValorTotal(valorTotal, pacote, subObjeto);
        }

        pacote.setValor(valorTotal);

        pacoteRepository.save(pacote);

        for (String cpf : pacoteDTO.getCpfsClientes()) {
            salvaClientePacote(pacoteDTO, pacote, cpf);
        }

        return valorTotal;
    }

    public List<PacoteEntity> todos() {
        return (List<PacoteEntity>) pacoteRepository.findAll();
    }

    public PacoteEntity porId(int id) {
        return pacoteRepository.findById(id).get();
    }

    public void salvaClientePacote(PacoteDTO pacoteDTO, PacoteEntity pacote, String cpf) {
        clientePacoteService.salvar(
                new ClientePacoteEntity(
                        clienteRepository.findByCpf(cpf),
                        pacote,
                        pacoteDTO.getQuantidadePacote()
                )
        );
    }

    public Double salvaEspacoPacoteRetornaValorTotal(Double valorTotal, PacoteEntity pacote, DadosDoPacotePorEspacoDTO subObjeto) {
        EspacoEntity espaco = espacoRepository.findByNome(subObjeto.getNomeEspaco());

        valorTotal += (espaco.getValor() * subObjeto.getQuantidade());

        espacosPacotesService.salvar(
                new EspacoPacoteEntity(
                        subObjeto.getTipoContratacao(),
                        subObjeto.getQuantidade(),
                        subObjeto.getPrazo(),
                        espaco,
                        pacote
                )
        );
        return valorTotal;
    }
}
