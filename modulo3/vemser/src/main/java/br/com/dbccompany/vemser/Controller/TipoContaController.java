package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Service.TipoContaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/tipoconta/todos
 * {{url}}/api/tipoconta/ver/{id}
 * {{url}}/api/tipoconta/novo
 * {{url}}/api/tipoconta/editar/{id}
 */

@Controller
@RequestMapping("api/tipoconta/")
public class TipoContaController extends ControllerAbstract<TipoContaService, TipoContaEntity, Integer> {
}
