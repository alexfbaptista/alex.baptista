import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DwarfBarbaLongaTeste {

    //como faco para testar a probabilidade ???
    @Test
    public void levaVariosDanos() {
        DwarfBarbaLonga zakk = new DwarfBarbaLonga("Zakk");
        ElfoVerde elfo = new ElfoVerde("Verr");
        while (zakk.getVida() >= 110.0 ) {
            elfo.atirarFlecha(zakk);
        }
        assertEquals(100.0, zakk.getVida(), 0);
    }
}
