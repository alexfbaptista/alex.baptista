package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.ContaClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContaClienteService extends ServiceAbstract<ContaClienteRepository, ContaClienteEntity, ContaClienteId> {

    public List<ContaClienteEntity> buscarTodosPorUsuario(UsuarioEntity usuario) {
        return repository.findAllByUsuario(usuario);
    }

    public List<ContaClienteEntity> buscarTodosPorBanco(int idBanco) {
        return repository.findAllByBanco(idBanco);
    }

    public List<ContaClienteEntity> buscarTodosPorConta(int idConta) {
        return repository.findAllByConta(idConta);
    }
}
