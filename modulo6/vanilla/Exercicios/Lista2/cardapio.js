function cardapioIFood(veggie = true, comLactose = false) {

      let resposta = []

      const cardapio = {
            comidas: ['enroladinho de salsicha', 'cuca de uva']
      }

      if (!comLactose) {
            cardapio.comidas.push('pastel de queijo')
      }

      resposta = cardapio.comidas.concat([
            'pastel de carne',
            'empada de legumes marabijosa'
      ])

      if (veggie) {

            const indiceEnroladinho = resposta.indexOf('enroladinho de salsicha')
            const indicePastelCarne = resposta.indexOf('pastel de carne')

            if (indiceEnroladinho == 0) {
                  resposta.splice(indiceEnroladinho, 1)
            } else {
                  resposta.splice(indiceEnroladinho - 1, 1)
            }

            if (indicePastelCarne == 0) {
                  esposta.splice(indicePastelCarne, 1)
            } else {
                  resposta.splice(indicePastelCarne - 1, 1)
            }
      }

      return resposta.map(comida => comida.toUpperCase());
}

console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]