package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Service.UsuarioService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/usuario/todos
 * {{url}}/api/usuario/ver/{id}
 * {{url}}/api/usuario/novo
 * {{url}}/api/usuario/editar/{id}
 */

@Controller
@RequestMapping("api/usuario/")
public class UsuarioController extends ControllerAbstract<UsuarioService, UsuarioEntity, Integer> {
}
