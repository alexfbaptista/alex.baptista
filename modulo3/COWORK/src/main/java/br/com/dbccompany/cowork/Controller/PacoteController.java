package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.PacoteDTO;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import br.com.dbccompany.cowork.Service.PacoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/pacote/")
public class PacoteController {

    @Autowired
    private PacoteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping(value = "/novo")
    @ResponseBody
    public String salvar(@RequestBody PacoteDTO pacoteDTO) {
        logger.info("requisicao (POST) api/pacote/novo sendo processada, data: " + LocalDate.now());
        try {

            return "R$ " + service.salvar(pacoteDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return "Pacote nao pode ser salvo";
        }
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<PacoteEntity> todos() {
        logger.info("requisicao (GET) api/pacote/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja pacotes registrados");

        return service.todos();
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public PacoteEntity buscarPorId(@PathVariable int id) {
        logger.info("requisicao (GET) api/pacote/buscar/por-id/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
