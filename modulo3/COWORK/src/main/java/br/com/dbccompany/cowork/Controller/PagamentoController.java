package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.PagamentoDTO;
import br.com.dbccompany.cowork.Entity.PagamentoEntity;
import br.com.dbccompany.cowork.Service.PagamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/pagamento/")
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping("/novo")
    @ResponseBody
    public PagamentoEntity salvar(@RequestBody PagamentoDTO pagamentoDTO) {
        logger.info("requisicao (POST) api/pagamento/novo sendo processada, data: " + LocalDate.now());
        try {

            return service.salvar(pagamentoDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<PagamentoEntity> todos() {
        logger.info("requisicao (GET) api/pagamento/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja pagamentos registrados");

        return service.todos();
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public PagamentoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/pagamento/buscar/porid/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-id/cliente-pacote/{id}")
    @ResponseBody
    public PagamentoEntity porIdClientePacote(@PathVariable int id) {
        logger.info("requisicao (GET) api/pagamento//buscar/por-id/cliente-pacote/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.buscarPorClientePacote(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-id/contratacao/{id}")
    @ResponseBody
    public PagamentoEntity porIdContratacao(@PathVariable int id) {
        logger.info("requisicao (GET) api/pagamento//buscar/por-id/cliente-pacote/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.buscarPorContratacao(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
