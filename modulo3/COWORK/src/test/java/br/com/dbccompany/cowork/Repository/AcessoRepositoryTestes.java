package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class AcessoRepositoryTestes {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    private ClienteEntity cliente;

    private EspacoEntity espaco;

    private SaldoClienteId saldoClienteId;

    private SaldoClienteEntity saldoCliente;

    @BeforeEach
    public void setup() {

        cliente = clienteRepository.save(
                new ClienteEntity("TesteC1", "22211144477", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("Sala X", 20, 250.0)
        );

        saldoClienteId = new SaldoClienteId(
                cliente.getId(),
                espaco.getId()
        );

        saldoCliente = saldoClienteRepository.save(
                new SaldoClienteEntity(saldoClienteId, TipoContratacaoEnum.DIARIA,
                        10, LocalDateTime.now().plusMonths(8))
        );

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );
    }

    @Test
    public void buscarPorSaldoClienteRetornaListDeTamanho5() {
        List<AcessoEntity> acessoEntities = acessoRepository.findBySaldoCliente(saldoCliente);
        assertEquals(5, acessoEntities.size());
    }
}
