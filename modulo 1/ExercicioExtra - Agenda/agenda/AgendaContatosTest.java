import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AgendaContatosTest {
    
    private AgendaContatos agenda;
    
    @Before
    public void setup() {
        agenda = new AgendaContatos();
        agenda.adicionar("Alex", "5555");
        agenda.adicionar("Gabriela", "8888");
        agenda.adicionar("Matheus", "7777");
    }

    @Test
    public void buscandoContatoPorNomeETelefone() {
        assertEquals("8888", agenda.buscar("Gabriela"));
        assertEquals("Gabriela", agenda.buscar("8888"));

    }

@Test
    public void pegandoCsvDaAgenda() {
        String separator = System.lineSeparator();
        String csvEsperado = String.format("Alex, 5555%sGabriela, 8888%sMatheus, 7777", separator, separator);

        assertEquals(csvEsperado, agenda.csv());
    }
}