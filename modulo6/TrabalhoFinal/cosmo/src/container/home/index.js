import React, { Component } from 'react';
import MensagemPromocao from '../../componentes/promocao';
import Header from '../../componentes/headerPrincipal';
import Banner from '../../componentes/banner';
import Colecao from '../../componentes/colecao';
import Api from '../../models/api';
import MenuNavegacaoResponsivo from '../../componentes/menuResponsivo';
import ProdutosPorCategoria from '../../models/produtosPorColecao';
import FooterPrincipal from '../../componentes/footer';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this._api = new Api();
    this.state = {
      podeCarregar: false
    }
  }

  componentDidMount() {
    const requisicoes = [
      this._api.buscarBanners(),
      this._api.buscarTodosProdutos(),
      this._api.buscarCategorias()
    ];
    Promise.all(requisicoes)
      .then( respostas => {
        this.setState(state => {
          return {
            ...state,
            banners: respostas[0].concat([]),
            produtos: respostas[1].concat([]),
            categorias: respostas[2].concat([])
          }
        })
        this.setState( state => {
          return {
            ...state,
            produtoCategoria: new ProdutosPorCategoria( this.state.categorias, this.state.produtos).formatar,
            podeCarregar: true
          }
        })
      })
    }

  render() {

    const { banners, produtoCategoria, podeCarregar } = this.state;

    return podeCarregar ? (
      <React.Fragment>
        <MensagemPromocao
          mensagem={'voa, frete, voa 🕊️ só R$ 3,99 - por tempo limitado nesse baaando de produtos'}
          cor={'promocao-roxo'}
          coluna={'col col-sm-12'}
        /> 
        <Header />
        <Banner banners={ banners } />
        <MenuNavegacaoResponsivo />
        {
          produtoCategoria && produtoCategoria.map( e => 
            <Colecao produtoCategoria={ produtoCategoria } colecao={ e.nome } mensagem={ e.subTitulo } produtos={ e.produtos } />
          )
        }
        <FooterPrincipal />
      </React.Fragment>
    ) : <h1>Aguarde!</h1>
  };
}
