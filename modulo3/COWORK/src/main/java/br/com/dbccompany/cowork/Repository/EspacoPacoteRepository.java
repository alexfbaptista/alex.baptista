package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {
    List<EspacoPacoteEntity> findAllByPacote(PacoteEntity pacote);
    List<EspacoPacoteEntity> findAllByEspaco(EspacoEntity espaco);
}
