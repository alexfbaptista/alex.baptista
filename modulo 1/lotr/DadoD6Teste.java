import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DadoD6Teste {

    @Test
    public void todosValoresEntre1e6() {
        DadoD6 d6 = new DadoD6();
        boolean soVeioNumeroEntre1e6 = true;
        for (int i = 0; i < 10000; i++) {
            int valor = d6.getDado();
            if (valor > 6 || valor <= 0) {
                soVeioNumeroEntre1e6 = false;
            }
        }
        assertTrue(soVeioNumeroEntre1e6);
    }
}
