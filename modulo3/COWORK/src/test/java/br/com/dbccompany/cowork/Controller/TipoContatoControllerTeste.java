package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Repository.TipoContatoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class TipoContatoControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TipoContatoRepository repository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper mapper;

    private TipoContatoEntity tipoContato;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mapper = new ObjectMapper();
        tipoContato = new TipoContatoEntity("email");
    }

    @Test
    public void deveRetornar200QuandoTipoContato() throws Exception {

        URI uri = new URI("/api/tipo-contato/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarTipoContatoPorId() throws Exception {

        URI uri = new URI("/api/tipo-contato/buscar/por-id/1");

        repository.save(new TipoContatoEntity("email"));

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("email")
                );
    }

    @Test
    public void retornarTipoContatoPorNome() throws Exception {

        URI uri = new URI("/api/tipo-contato/buscar/por-tipo/email");

        repository.save(new TipoContatoEntity("email"));

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("email")
                );
    }

    @Test
    public void salvarERetornarTipoContato() throws Exception {

        URI uri = new URI("/api/tipo-contato/novo");
        Set<ContatoEntity> contatos = new HashSet<>();
        tipoContato.setContatos(contatos);

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tipoContato);

        System.out.println(json);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("email")
                );
    }

    @Test
    public void retornarTipoContatoAoAtualizar() throws Exception {

        URI uri = new URI("/api/tipo-contato/editar/1");
        Set<ContatoEntity> contatos = new HashSet<>();
        tipoContato.setContatos(contatos);

        repository.save(tipoContato);

        tipoContato.setNome("emailX");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tipoContato);

        System.out.println(json);
        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("emailX")
                );
    }
}
