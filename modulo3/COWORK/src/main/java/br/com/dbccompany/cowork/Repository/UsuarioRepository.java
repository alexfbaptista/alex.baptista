package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    Optional<UsuarioEntity> findByLogin(String login);
    UsuarioEntity findByEmail(String email);
}
