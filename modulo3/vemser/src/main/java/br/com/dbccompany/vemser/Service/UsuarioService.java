package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    public UsuarioEntity buscarPorCpf(Date cpf) {
        return repository.findByCpf(cpf);
    }

    public List<UsuarioEntity> buscarTodosPorNome(String nome) {
        return repository.findAllByNome(nome);
    }

    public List<UsuarioEntity> buscarTodosPorEstadoCivil(String estadoCivil) {
        return repository.findAllByEstadoCivil(estadoCivil);
    }

    @Override
    public UsuarioEntity salvar(UsuarioEntity entidade) {
        entidade.setSenha(new BCryptPasswordEncoder().encode(entidade.getPassword()));
        return super.salvar(entidade);
    }
}
