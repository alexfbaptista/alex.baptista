class Jedi {
    constructor(nome) {
        this._nome = nome;
        this._estaMorto = false;
    }

    matarJedi() {
        this._estaMorto = true;
    }

    atacarComSabre() {
        setTimeout( () => {
            console.log("Ataquei");
        }, 2000 );
    }

    get nome() {
        return this._nome;
    }

    get estaMorto() {
        return this._estaMorto;
    }
}

let sky = new Jedi("Luke");
sky.atacarComSabre();
sky.matarJedi();

if(sky.estaMorto){
    console.log("Ele morreeeeeeeeeeeu");
}