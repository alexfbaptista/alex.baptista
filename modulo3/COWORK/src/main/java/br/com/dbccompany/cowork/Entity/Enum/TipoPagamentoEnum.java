package br.com.dbccompany.cowork.Entity.Enum;

public enum TipoPagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
