import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MaioriaDeElfosVerdesTeste {

    ElfoVerde ev1;
    ElfoVerde ev2;
    ElfoVerde ev3;
    ElfoVerde ev4;
    ElfoVerde ev5;
    ElfoVerde ev6;
    ElfoVerde ev7;
    ElfoNoturno en1;
    ElfoNoturno en2;
    ElfoNoturno en3;
    ExercitoDeElfos exercito;
    MaioriaDeElfosVerdes estrategia;

//    @Test
//    public void composicaoCom7VerdesE2NoturnosRetornaVerdadeiro() {
//        assertTrue(estrategia.composicao());
//    }

    @Test
    public void ordenarPorQtdFlecha() {
        ElfoVerde ev8 = new ElfoVerde("ev8");
        ev8.atirarFlecha(new Dwarf("Dummy Target"));
        exercito.alistar(ev8);

        estrategia = new MaioriaDeElfosVerdes(exercito);

        assertEquals(ev8, estrategia.pelotaoApto().get(0));

    }



    @Before
    public void setup() {

        ev1 = new ElfoVerde("ev1");
        ev2 = new ElfoVerde("ev2");
        ev3 = new ElfoVerde("ev3");
        ev4 = new ElfoVerde("ev4");
        ev5 = new ElfoVerde("ev5");
        ev6 = new ElfoVerde("ev6");
        ev7 = new ElfoVerde("ev7");
        en1 = new ElfoNoturno("en1");
        en2 = new ElfoNoturno("en1");
        en3 = new ElfoNoturno("en1");

        exercito = new ExercitoDeElfos();

        exercito.alistar(ev1);
        exercito.alistar(ev2);
        exercito.alistar(ev3);
        exercito.alistar(ev4);
        exercito.alistar(ev5);
        exercito.alistar(ev6);
        exercito.alistar(ev7);
        exercito.alistar(en1);
        exercito.alistar(en2);
        exercito.alistar(en3);

        estrategia = new MaioriaDeElfosVerdes(exercito);
    }
}
