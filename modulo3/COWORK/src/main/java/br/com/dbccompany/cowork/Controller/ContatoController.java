package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.ContatoDTO;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Service.ContatoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/contato/")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public ContatoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/contato/buscar/por-id/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    contatoService.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<ContatoEntity> todos() {
        logger.info("requisicao (GET) api/contato/buscar/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja contatos registrados");

        return contatoService.todos();
    }

    @GetMapping(value = "/buscar/por-tipo-contato/{tipo}")
    @ResponseBody
    public List<ContatoEntity> porTipoContato(@PathVariable String tipo) {
        logger.info("requisicao (GET) api/contato/buscar/por-tipo-contato/{tipo} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("Tipo passado na requisicao: " + tipo);
        try {

            return Optional.ofNullable(
                    contatoService.todosPorTipoContato(tipo)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-cliente/{cpf}")
    @ResponseBody
    public List<ContatoEntity> porCliente(@PathVariable String cpf) {
        logger.info("requisicao (GET) api/contato/buscar/por-cliente/{cpf} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("CPF passado na requisicao: " + cpf);
        try {

            return Optional.ofNullable(
                    contatoService.todosPorCliente(cpf)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContatoEntity novo(@RequestBody ContatoDTO contatoDTO) {
        logger.info("requisicao (POST) api/contato/novo " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return contatoService.novoContato(contatoDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContatoEntity editar(@RequestBody ContatoDTO contatoDTO, @PathVariable int id) {
        logger.info("requisicao (PUT) api/contato/editar/{id} " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return contatoService.editarContato(contatoDTO, id);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-valor")
    @ResponseBody
    public ContatoEntity porValor(@RequestBody ContatoDTO contatoDTO) {
        logger.info("requisicao (GET) api/contato/buscar/por-cliente/{cpf} " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return contatoService.buscarPorValor(contatoDTO.getValorContato());

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
