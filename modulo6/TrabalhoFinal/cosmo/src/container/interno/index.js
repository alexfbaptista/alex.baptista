import React, { Component } from 'react';
import Colecao from '../../componentes/colecao';
import Header from '../../componentes/headerPrincipal';
import Informativo from '../../componentes/paginaInternaInformativo';
import DadosVendedor from '../../componentes/paginaInternaVendedor';
import ProdutoDetalhado from '../../componentes/produtoPaginaInterna';
import MensagemPromocao from '../../componentes/promocao';
import Api from '../../models/api';
import Comentarios from '../../componentes/paginaInternaComentarios';
import ProdutosPorCategoria from '../../models/produtosPorColecao';
import FooterPrincipal from '../../componentes/footer';
import PreFooterInterno from '../../componentes/preFooter';

export default class PaginaInterna extends Component {

  constructor(props) {
    super(props);
    this._api = new Api();
    this.state = {
      produto : this.props.location.state.produto,
      produtoCategoria: this.props.location.state.produtoCategoria,
      podeCarregar: false
    }
  }

  componentDidMount() {
    const { produto } = this.state;
    const requisicoes = [
      this._api.buscarDetalhesProduto( produto.id ),
      this._api.buscarVendedor( produto.idVendedor ),
      this._api.buscarTodosProdutos(),
      this._api.buscarComentario( produto.id ),
      this._api.buscarCategorias()
    ];
    Promise.all(requisicoes)
      .then( respostas => {
        this.setState(state => {
          return {
            ...state,
            detalhes: respostas[0],
            vendedor: respostas[1],
            produtos: respostas[2],
            comentarios: respostas[3],
            produtoCategoria: new ProdutosPorCategoria( respostas[4], respostas[2] ).formatar,
            podeCarregar: true
          }
        })
      })
    }

    onSearch = comentario => {
      const { produto } = this.state;
      this._api.registrarComentario( produto.id, comentario, 'User' );
      window.location.reload();
    };

    comentariosTela( comentarios ) {
      let tamanho = comentarios.length;
      if( tamanho >= 7 ) {
        return [ 
          comentarios[tamanho - 7], comentarios[tamanho - 6], comentarios[tamanho - 5],
          comentarios[tamanho - 4], comentarios[tamanho - 3], comentarios[tamanho - 2], 
          comentarios[tamanho - 1] 
        ]
      } else {
        return comentarios
      }
    }

    atualizarTela() {
      window.location.reload();
    }

  render() {

    const { produto, detalhes, vendedor, comentarios, podeCarregar, produtoCategoria } = this.state;

    return podeCarregar ? (
      <React.Fragment>
        <MensagemPromocao
          mensagem={'🚚 frete grátis na primeira compra - promo válida no frete padrão até R$20'}
          cor={'promocao-roxo'}
          coluna={'col col-sm-12'}
        /> 
        <Header />
        <ProdutoDetalhado 
          produto={ produto }  
          detalhes={ detalhes }
          vendedor={ vendedor }
        />
        <div className='row'>
          <Comentarios 
            metodo={ this.onSearch } 
            comentarios={ this.comentariosTela( comentarios ) } 
          />
          <DadosVendedor 
            vendedor={ vendedor }
          />
        </div>
        <Informativo />
        {
          produtoCategoria && produtoCategoria.map( e => 
            <Colecao metodo={ this.atualizarTela } classe={ 'pagina-interna-colecao' } colecao={ e.nome } mensagem={ e.subTitulo } produtos={ e.produtos } />
          )
        }
        <PreFooterInterno />
        <FooterPrincipal />
      </React.Fragment>
    ) : <h1>Aguarde!</h1>
  };
}
