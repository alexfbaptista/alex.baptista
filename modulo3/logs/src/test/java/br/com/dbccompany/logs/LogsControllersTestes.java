package br.com.dbccompany.logs;

import br.com.dbccompany.logs.Repository.LogsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
public class LogsControllersTestes {

    @Autowired
    private LogsRepository logsRepository;

    @Autowired
    private MockMvc mockMvc;

//    @Test
//    public void deveRetornar200AoChamarTodosLogs() throws Exception {
//        URI uri = new URI("/logs/todos");
//        mockMvc
//                .perform(MockMvcRequestBuilders
//                        .get(uri)
//                        .contentType(MediaType.APPLICATION_JSON)
//                ).andExpect(MockMvcResultMatchers
//                .status()
//                .is(200)
//        );
//    }

    @Test
    public void salvarERetornarUmEspaco() throws Exception {

        URI uri = new URI("/logs/novo");

        String json = "{\"logText\": \"texto de log teste texto de log teste texto de log teste texto de log teste\"}";

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.logText")
                                .value("{\"logText\": \"texto de log teste texto de log teste texto de log teste texto de log teste\"}")
                );
    }
}
