package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.NomeEspacoDTO;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Service.EspacosPacotesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/espaco-pacote/")
public class EspacoPacoteController {

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "todos")
    @ResponseBody
    public List<EspacoPacoteEntity> todos() {
        logger.info("requisicao (GET) api/espaco-pacote/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja " +
                "espaco-pacote registrados");

        return espacosPacotesService.todos();
    }

    @GetMapping("/buscar/por-id/{id}")
    @ResponseBody
    public EspacoPacoteEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/espaco-pacote/buscar/porid/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    espacosPacotesService.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping("/buscar/por-espaco")
    @ResponseBody
    public List<EspacoPacoteEntity> porEspaco(@RequestBody NomeEspacoDTO dto) {
        logger.info("requisicao (GET) api/espaco-pacote/buscar/por-espaco " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("Nome passado na requisicao: " + dto.getNome());
        try {

            return espacosPacotesService.todosPorEspaco(dto.getNome());

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping("/buscar/por-id-pacote/{id}")
    @ResponseBody
    public List<EspacoPacoteEntity> porPacote(@PathVariable int id) {
        logger.info("requisicao (GET) api/espaco-pacote/buscar/por-id-pacote/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return espacosPacotesService.todosPorPacote(id);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
