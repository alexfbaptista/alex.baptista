import React from 'react';
import logo from '../../imagens/logo/logo.jpg';
import { Link } from 'react-router-dom';
import BotaoUi from '../botaoUi';

const FooterPrincipal = ({  }) =>
    <React.Fragment>
        <div className='row'>
            <footer className='footer'>
                <div className='col col-sm-1 logo-footer-container'>
                    <Link to='/'>
                        <img className='logo-footer' src={ logo } alt='logo' />
                    </Link>
                </div>
                <div className='col col-sm-5 footer-text-direitos'>
                    <span>enjoei © 2020 - todos os direitos reservados - enjoei.com.br atividades de internet S.A. CNPJ: 16.922.038/0001-51
                            av. isaltino victor de moraes, 437, vila bonfim, embu das artes, sp, 06806-400 - (11) 3197-4883</span>
                </div>
                <div className='col col-sm-6 botao-footer-container'>
                    <BotaoUi classe='botao-footer' nome='baixe o enjuapp' />
                </div>
            </footer>
        </div>
    </React.Fragment>

export default FooterPrincipal;