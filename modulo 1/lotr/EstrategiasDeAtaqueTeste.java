import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EstrategiasDeAtaqueTeste {

    ElfoVerde ev1;
    ElfoVerde ev2;
    ElfoVerde ev3;
    ElfoVerde ev4;
    ElfoNoturno en1;
    ElfoNoturno en2;
    ElfoNoturno en3;
    ElfoNoturno en4;
    ExercitoDeElfos exercito;
    EstrategiasDeAtaque estrategia;

    @Test
    public void tamanhoDaListasDeElfoVerdeAptosDeveSer4() {
        assertEquals(3, estrategia.ordensElfosVerdes().size());
    }

    @Test
    public void tamanhoDaListaDeElfosNoturnosAptosDeveSer2() {
        assertEquals(2, estrategia.ordensElfosNoturnos().size());
    }

    @Before
    public void setup() {

        ev1 = new ElfoVerde("ev1");
        ev2 = new ElfoVerde("ev2");
        ev3 = new ElfoVerde("ev3");
        ev4 = new ElfoVerde("ev4");
        en1 = new ElfoNoturno("en1");
        en2 = new ElfoNoturno("en1");
        en3 = new ElfoNoturno("en1");
        en4 = new ElfoNoturno("en1");

        ev2.atirarFlecha(new Dwarf("Dummy Target"));
        ev2.atirarFlecha(new Dwarf("Dummy Target"));

        en2.atirarFlecha(new Dwarf("Dummy Target"));
        en2.atirarFlecha(new Dwarf("Dummy Target"));
        en3.atirarFlecha(new Dwarf("Dummy Target"));
        en3.atirarFlecha(new Dwarf("Dummy Target"));

        exercito = new ExercitoDeElfos();

        exercito.alistar(ev1);
        exercito.alistar(ev3);
        exercito.alistar(en1);
        exercito.alistar(ev4);
        exercito.alistar(en2);
        exercito.alistar(en3);
        exercito.alistar(en4);
        exercito.alistar(ev2);

        estrategia = new EstrategiasDeAtaque(exercito) {
            @Override
            void atacar(Dwarf dwarf) {

            }

            @Override
            boolean composicao() {
                return false;
            }
        };
    }
}
