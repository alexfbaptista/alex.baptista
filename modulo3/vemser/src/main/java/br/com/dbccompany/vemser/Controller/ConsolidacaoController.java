package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Service.ConsolidacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/consolidacao/todos  -> Retornando Status 500
 * {{url}}/api/consolidacao/ver/{id}
 * {{url}}/api/consolidacao/novo
 * {{url}}/api/consolidacao/editar/{id}
 */

@Controller
@RequestMapping("/api/consolidacao/")
public class ConsolidacaoController extends ControllerAbstract<ConsolidacaoService, ConsolidacaoEntity, Integer>{
}
