package br.com.dbccompany.cowork.DTO;

import java.util.List;

public class PacoteDTO {

    private List<String> cpfsClientes;
    private List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS;
    private int quantidadePacote;

    public PacoteDTO() {
    }

    public PacoteDTO(List<String> cpfsClientes, List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS) {
        this.cpfsClientes = cpfsClientes;
        this.dadosDoPacotePorEspacoDTOS = dadosDoPacotePorEspacoDTOS;
    }

    public PacoteDTO(List<String> cpfsClientes, List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS, int quantidadePacote) {
        this.cpfsClientes = cpfsClientes;
        this.dadosDoPacotePorEspacoDTOS = dadosDoPacotePorEspacoDTOS;
        this.quantidadePacote = quantidadePacote;
    }

    public List<String> getCpfsClientes() {
        return cpfsClientes;
    }

    public void setCpfsClientes(List<String> cpfsClientes) {
        this.cpfsClientes = cpfsClientes;
    }

    public List<DadosDoPacotePorEspacoDTO> getPacoteSubparteDTOS() {
        return dadosDoPacotePorEspacoDTOS;
    }

    public void setPacoteSubparteDTOS(List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS) {
        this.dadosDoPacotePorEspacoDTOS = dadosDoPacotePorEspacoDTOS;
    }

    public List<DadosDoPacotePorEspacoDTO> getPacoteObjetoParteDoDTOS() {
        return dadosDoPacotePorEspacoDTOS;
    }

    public void setPacoteObjetoParteDoDTOS(List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS) {
        this.dadosDoPacotePorEspacoDTOS = dadosDoPacotePorEspacoDTOS;
    }

    public int getQuantidadePacote() {
        return quantidadePacote;
    }

    public void setQuantidadePacote(int quantidadePacote) {
        this.quantidadePacote = quantidadePacote;
    }
}
