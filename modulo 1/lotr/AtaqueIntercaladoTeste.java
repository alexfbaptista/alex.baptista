import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AtaqueIntercaladoTeste {

    ElfoVerde ev1;
    ElfoVerde ev2;
    ElfoVerde ev3;
    ElfoVerde ev4;
    ElfoNoturno en1;
    ElfoNoturno en2;
    ElfoNoturno en3;
    ElfoNoturno en4;
    ExercitoDeElfos exercito;
    AtaqueIntercalado estrategia;

    @Test
    public void todosOsElfosIntercalados() {
        assertEquals(ev1, estrategia.fileiraDeAtaque().get(0));
        assertEquals(en1, estrategia.fileiraDeAtaque().get(1));
        assertEquals(ev3, estrategia.fileiraDeAtaque().get(2));
        assertEquals(en2, estrategia.fileiraDeAtaque().get(3));
        assertEquals(ev4, estrategia.fileiraDeAtaque().get(4));
        assertEquals(en3, estrategia.fileiraDeAtaque().get(5));
        assertEquals(ev2, estrategia.fileiraDeAtaque().get(6));
        assertEquals(en4, estrategia.fileiraDeAtaque().get(7));
    }

    @Test
    public void composicaoMeioAMeioRetornaTrue() {
        assertTrue(estrategia.composicao());
    }

    @Test
    public void composicaoCom1ElfoVerdeMaisQueNoturnoRetornaFalso() {
        exercito.alistar(new ElfoVerde("ev5"));
        estrategia = new AtaqueIntercalado(exercito);
        assertFalse(estrategia.composicao());
    }

    @Before
    public void setup() {

        ev1 = new ElfoVerde("ev1");
        ev2 = new ElfoVerde("ev2");
        ev3 = new ElfoVerde("ev3");
        ev4 = new ElfoVerde("ev4");
        en1 = new ElfoNoturno("en1");
        en2 = new ElfoNoturno("en1");
        en3 = new ElfoNoturno("en1");
        en4 = new ElfoNoturno("en1");

        exercito = new ExercitoDeElfos();

        exercito.alistar(ev1);
        exercito.alistar(ev3);
        exercito.alistar(en1);
        exercito.alistar(ev4);
        exercito.alistar(en2);
        exercito.alistar(en3);
        exercito.alistar(en4);
        exercito.alistar(ev2);

        estrategia = new AtaqueIntercalado(exercito);
    }
}
