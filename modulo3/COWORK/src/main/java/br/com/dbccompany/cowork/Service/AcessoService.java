package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.DTO.AcessoDTO;
import br.com.dbccompany.cowork.DTO.CPFClienteEspacoNomeDTO;
import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Exceptions.*;
import br.com.dbccompany.cowork.Repository.AcessoRepository;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.SaldoClienteRepository;
import br.com.dbccompany.cowork.Utils.Datas.CalculaDescontoEntradaSaida;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService extends ServiceAbstract<AcessoRepository, AcessoEntity, Integer> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public AcessoEntity salvarEntrada(AcessoDTO acessoDTO) throws ClienteNaoEncontradoExcecao, EspacoNaoEncontradoException,
            SaldoClienteNaoEncontradoException, PacoteEstaVencidoException, SaldoInsuficienteException {

        Optional<ClienteEntity> cliente = Optional.ofNullable(clienteRepository.findByCpf(acessoDTO.getCpf()));

        if (cliente.isPresent()) {

            Optional<EspacoEntity> espaco = Optional.ofNullable(espacoRepository.findByNome(acessoDTO.getNomeEspaco()));

            if (espaco.isPresent()) {

                Optional<SaldoClienteEntity> saldoCliente = saldoClienteRepository.findById(
                        new SaldoClienteId(
                                cliente.get().getId(),
                                espaco.get().getId()
                        )
                );

                if (saldoCliente.isPresent()) {

                    if (saldoCliente.get().getQuantidade() <= 0) {
                        throw new SaldoInsuficienteException(String.valueOf(saldoCliente.get().getQuantidade()));
                    }

                    if (acessoDTO.getDataDeAcesso() == null) {
                        acessoDTO.setDataDeAcesso(LocalDateTime.now());
                    }

                    AcessoEntity acesso = new AcessoEntity(
                            saldoCliente.get(),
                            true,
                            acessoDTO.getDataDeAcesso(),
                            false
                    );

                    if (saldoCliente.get().getVencimento().compareTo(acesso.getDataDoAcesso()) >= 0) {

                        repository.save(acesso);

                        return acesso;

                    } else {

                        saldoCliente.get().setQuantidade(0);
                        saldoClienteRepository.save(saldoCliente.get());

                        throw new PacoteEstaVencidoException(String.valueOf(saldoCliente.get().getVencimento()));
                    }
                }

                throw new SaldoClienteNaoEncontradoException("cpf " + acessoDTO.getCpf() + " e espaco nome" + acessoDTO.getNomeEspaco());
            }

            throw new EspacoNaoEncontradoException(acessoDTO.getNomeEspaco());
        }

        throw new ClienteNaoEncontradoExcecao(acessoDTO.getCpf());
    }

    @Transactional(rollbackFor = Exception.class)
    public AcessoEntity salvarSaida(AcessoDTO acessoDTO) throws ClienteNaoEncontradoExcecao, EspacoNaoEncontradoException,
            SaldoClienteNaoEncontradoException, AcessoNaoEncontradoException {

        Optional<ClienteEntity> cliente = Optional.ofNullable(clienteRepository.findByCpf(acessoDTO.getCpf()));

        if (cliente.isPresent()) {

            Optional<EspacoEntity> espaco = Optional.ofNullable(espacoRepository.findByNome(acessoDTO.getNomeEspaco()));

            if (espaco.isPresent()) {
                Optional<SaldoClienteEntity> saldoCliente = saldoClienteRepository.findById(
                        new SaldoClienteId(
                                cliente.get().getId(),
                                espaco.get().getId()
                        )
                );

                if (saldoCliente.isPresent()) {

                    List<AcessoEntity> acessos = repository.findBySaldoCliente(saldoCliente.get());

                    acessos.sort(Comparator.comparing(AcessoEntity::getId));

                    if (!acessos.isEmpty()) {

                        defineNovoSaldoDescontadoConformeTipoContratacao(saldoCliente, acessos);

                        saldoClienteRepository.save(saldoCliente.get());

                        return repository.save(
                                new AcessoEntity(
                                    saldoCliente.get(),
                                        false,
                                        LocalDateTime.now(),
                                        false
                                )
                        );
                    }

                    throw new AcessoNaoEncontradoException("cliente de cpf" + acessoDTO.getCpf() + "e espaco com nome " + acessoDTO.getNomeEspaco());
                }

                throw new SaldoClienteNaoEncontradoException("cpf " + acessoDTO.getCpf() + " e espaco nome" + acessoDTO.getNomeEspaco());
            }

            throw new EspacoNaoEncontradoException(acessoDTO.getNomeEspaco());
        }

        throw new ClienteNaoEncontradoExcecao(acessoDTO.getCpf());
    }

    public void defineNovoSaldoDescontadoConformeTipoContratacao(Optional<SaldoClienteEntity> saldoCliente, List<AcessoEntity> acessos) {

        CalculaDescontoEntradaSaida desconto = new CalculaDescontoEntradaSaida(
                acessos.get(acessos.size() - 1).getDataDoAcesso(),
                LocalDateTime.now()
        );

        int saldoAtual = saldoCliente.get().getQuantidade();

        switch (saldoCliente.get().getTipoContratacao()) {

            case MINUTO:
                saldoCliente
                        .get()
                        .setQuantidade(
                               (saldoAtual - desconto.getMinutes())
                        );
                break;

            case HORA:
                saldoCliente
                        .get()
                        .setQuantidade(
                                (saldoAtual - desconto.getHours())
                        );
                break;

            case DIARIA:
                saldoCliente
                        .get()
                        .setQuantidade(
                                (saldoAtual - desconto.getDays())
                        );
                break;

            case TURNO:
                saldoCliente
                        .get()
                        .setQuantidade(
                                (saldoAtual - desconto.getTurnos())
                        );
                break;

            case SEMANA:
                saldoCliente
                        .get()
                        .setQuantidade(
                                (saldoAtual - desconto.getSemanas())
                        );
                break;

            case MES:
                saldoCliente
                        .get()
                        .setQuantidade(
                                (saldoAtual - desconto.getMonths())
                        );
                break;
        }
    }

    public List<AcessoEntity> buscarAcessosPorSaldoCliente(CPFClienteEspacoNomeDTO  cpfClienteEspacoNomeDTO) {

        return repository
                .findBySaldoCliente(
                        saldoClienteRepository.findById(
                                new SaldoClienteId(
                                        clienteRepository.findByCpf(cpfClienteEspacoNomeDTO.getCpfCliente()).getId(),
                                        espacoRepository.findByNome(cpfClienteEspacoNomeDTO.getEspacoNome()).getId()
                                )
                        ).get()
                );
    }
}
