/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */


// eslint-disable-next-line no-var
var contadorDoBotaoDeSalvamento = 1;

function salvarPokemon() {
  if ( this.contadorDoBotaoDeSalvamento >= 9 ) {
    this.contadorDoBotaoDeSalvamento = 1;
  }

  if ( !Number.isNaN( this.idBuscadoNaUltimaRequisicao ) ) {
    // eslint-disable-next-line prefer-const
    let status = dadosPokemon.querySelector( `.salvar${ this.contadorDoBotaoDeSalvamento }` );
    status.innerHTML = this.idBuscadoNaUltimaRequisicao;

    this.contadorDoBotaoDeSalvamento += 1;
  }
}

function dispararIDNoSalvar1() {
  const id = document.getElementById( 'salvar1' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar2() {
  const id = document.getElementById( 'salvar2' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar3() {
  const id = document.getElementById( 'salvar3' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar4() {
  const id = document.getElementById( 'salvar4' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar5() {
  const id = document.getElementById( 'salvar5' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar6() {
  const id = document.getElementById( 'salvar6' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar7() {
  const id = document.getElementById( 'salvar7' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}

function dispararIDNoSalvar8() {
  const id = document.getElementById( 'salvar8' ).innerHTML;
  this.idBuscadoNaUltimaRequisicao = id;
  getInputValue( id );
}
