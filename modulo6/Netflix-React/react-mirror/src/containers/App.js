import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './home';
import ListaAvaliacoes from './avaliacoes';
import DetalhesEpisodios from './detalhesEpisodios';
import CadastroUsuario from './cadastroUsuario';
import Login from './login';
import { PrivateRoute } from '../components/rotaPrivada';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <Router>
          <Route path='/cadastro' exact component={ CadastroUsuario } />
          <Route path='/login' exact component={ Login } />
          <PrivateRoute path="/home" exact component={ Home } />
          <PrivateRoute path="/episodio/:id" exact component={ DetalhesEpisodios } />
          <PrivateRoute path="/avaliacoes" exact component={ ListaAvaliacoes } />
        </Router>
      </div>
    )
  };
}

/* const ListaAvaliacoes = () => <h2>Lista Avaliações</h2> */