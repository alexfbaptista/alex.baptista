package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.AcessoEntity;
import br.com.dbccompany.cowork.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
    List<AcessoEntity> findBySaldoCliente(SaldoClienteEntity saldoCliente);
}
