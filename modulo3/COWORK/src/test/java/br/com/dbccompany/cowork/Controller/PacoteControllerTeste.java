package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.DadosDoPacotePorEspacoDTO;
import br.com.dbccompany.cowork.DTO.PacoteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.PacoteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class PacoteControllerTeste {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private PacoteEntity pacote;

    private ObjectMapper mapper;

    private PacoteDTO pacoteDTO;

    private EspacoEntity espaco;

    private ClienteEntity cliente;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mapper = new ObjectMapper();

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 40, 350.0)
        );

        List<String> cpfs = new ArrayList<>();
        cpfs.add("44477788822");

        List<DadosDoPacotePorEspacoDTO> dadosDoPacotePorEspacoDTOS = new ArrayList<>();
        dadosDoPacotePorEspacoDTOS.add(
                new DadosDoPacotePorEspacoDTO("SalaTeste", TipoContratacaoEnum.DIARIA, 20, 30)
        );

        pacoteDTO = new PacoteDTO(cpfs, dadosDoPacotePorEspacoDTOS, 10);
    }

    @Test
    public void deveRetornar200QuandoPacote() throws Exception {
        URI uri = new URI("/api/pacote/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarPacote() throws Exception {

        URI uri = new URI("/api/pacote/novo");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pacoteDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .content().string("R$ 7000.0")
                );
    }

    @Test
    public void retornarPacotePorId() throws Exception {

        URI uri = new URI("/api/pacote/buscar/por-id/1");

        pacoteRepository.save(new PacoteEntity(800.0));

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value(800.0)
                );
    }
}