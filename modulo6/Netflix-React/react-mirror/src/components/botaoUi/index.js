import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './botao.css'

const BotaoUi = ( { classe, metodo, nome, link } ) =>
  <React.Fragment>
    <button className={`botao ${ classe }`} onClick={ metodo } >
      <span>
        { link ? <Link to={ link }>{ nome }</Link> : nome }
      </span>
    </button>
  </React.Fragment>

BotaoUi.propTypes = {
  nome: PropTypes.string.isRequired,
  metodo: PropTypes.func,
  classe: PropTypes.string
}

export default BotaoUi;