// Exercício 01
// Vamos criar um sistema para uma liga local (não importa se é esporte ou e-Sports), 
// na a primeira entrega que o cliente quer ver é a possibilidade de criar os times (nome, tipo de esporte, status, liga que joga), 
// adicionar os jogadores, buscar os jogadores por nome e por número (todos jogadores devem ter um número de camiseta).

// Exercício 02
// Além disso o cliente quer ver o histórico de partidas do time.

class Jogador {
    constructor(nome, numero) {
        this._nomeJogador = nome;
        this._numeroJogador = numero;
    }
}

//Como 1 time pode disputar N campeonatos diferentes no mundo me fez sentido o time ser responsável pelo seu próprio histórico de jogos
//E campeonatos responsáveis por seu histórico de jogos disputados
class Time {
    constructor(nome, tipoDeEsporte, status, ligaQueJoga) {
        this._nome = nome;
        this._tipoDeEsporte = tipoDeEsporte;
        this._status = status;
        this._ligaQueJoga = ligaQueJoga;
        this._jogadores = [];
        this._historicoDeResultadoDasPartidas = []; 
    }

    get getJogadores() {
        return this._jogadores;
    }

    get getHistoricoDePartidas() {
        return this._historicoDeResultadoDasPartidas;
    }

    adicionarJogador(nome, numero) {
        this._jogadores.push(new Jogador(nome, numero));
    }

    buscarJogadorPorNome(nome) {
        return this._jogadores.filter(jogador => jogador._nomeJogador == nome)[0];
    }

    buscarJogadorPorNumero(numero) {
        return this._jogadores.filter(jogador => jogador._numeroJogador == numero)[0];
    }

    adicionarPartidaAoHistorico(partida) {
        this._historicoDeResultadoDasPartidas.push(partida);
    }
}

class Partida {
    constructor(nomeTime1, nomeTime2) {
        this._nomeTime1 = nomeTime1;
        this._nomeTime2 = nomeTime2;
        this._pontosTime1 = 0;
        this._pontosTime2 = 0;
        this._isPartidaFinalizada = false;  
    }

    get times() {
        return times = [this._nomeTime1, this.time2];
    }

    get time1() {
        return this._nomeTime1;
    }

    get time2() {
        return this._nomeTime2;
    }

    finalizarPartida() {
        this._isPartidaFinalizada = true;
    }

    pontuarTime1() {
        if (!this._isPartidaFinalizada) {
            this._pontosTime1++;
        }
    }

    pontuarTime2() {
        if (!this._isPartidaFinalizada) {
            this._pontosTime2++;
        }
    }

    resultado() {
        return (this._pontosTime1 >= this._pontosTime2) 
            ? `${this._nomeTime1}: ${this._pontosTime1} x ${this._pontosTime2} :${this._nomeTime2}` 
                : `${this._nomeTime2}: ${this._pontosTime2} x ${this._pontosTime1} :${this._nomeTime1}`
    }
}


//Codigo fora do exercício
class Campeonato {
    constructor(nomeDoCampeonato) {
        this._nomeDoCampeonato = nomeDoCampeonato;
        this._historicoDePartidasDisputadasNoCampeonato = [];
        this._timesInscritos = [];
    }

    registrarTime(time) {
        this._timesInscritos.push(time);
    }

    registrarPartida(partida) {
        this._historicoDePartidasDisputadasNoCampeonato.push(partida);
    }
}


//Testes
let inter = new Time("Internacional", "Futebol", "Ativo", "Primeira Divisão");

inter.adicionarJogador("Thiago Galhardo", 17);
inter.adicionarJogador("Dourado", 13);
inter.adicionarJogador("D'Alessandro", 10);

console.log(inter.getJogadores);

console.log(inter.buscarJogadorPorNome("Dourado"));
console.log(inter.buscarJogadorPorNome("Guinazu"));

console.log(inter.buscarJogadorPorNumero(17));
console.log(inter.buscarJogadorPorNumero(20));
console.log(inter.buscarJogadorPorNumero(13));
console.log(inter.buscarJogadorPorNumero(05));
console.log(inter.buscarJogadorPorNumero(10));

let gremio = new Time("Gremio Timinho", "Futebol", "Ativo", "Primeira Divisão");
gremio.adicionarJogador("Pepe", 25);
gremio.adicionarJogador("Geromel", 03);
gremio.adicionarJogador("Pyerre", 21);

let finalGauchao = new Partida(inter._nome, gremio._nome);

finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();
finalGauchao.pontuarTime1();

finalGauchao.pontuarTime2();

finalGauchao.finalizarPartida();

finalGauchao.pontuarTime2(); //nao vai pontuar

inter.adicionarPartidaAoHistorico(finalGauchao);
gremio.adicionarPartidaAoHistorico(finalGauchao);

console.log(inter.getHistoricoDePartidas);









