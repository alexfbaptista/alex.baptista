import EpisodioUi from './episodioUi';
import BotaoUi from './botaoUi';
import MensagemFlash from './mensagemFlash';
import MeuInputNumero from './meuInputNumero';
import Lista from './lista';
import ListaEpisodiosUi from './listaEpisodiosUi';
import CampoBusca from './campoBusca';
import NetflixHeader from './header'

export { EpisodioUi, BotaoUi, MensagemFlash, MeuInputNumero, Lista, ListaEpisodiosUi, CampoBusca, NetflixHeader };