import EpisodiosApi from './episodiosAPI';
import ListaEpisodios from './listaEpisodios';
import Episodio from './episodio'

export { EpisodiosApi, ListaEpisodios, Episodio };