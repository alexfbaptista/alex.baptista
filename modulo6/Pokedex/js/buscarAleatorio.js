// eslint-disable-next-line no-unused-vars
function buscarAleatorio() {
  const numeroAleatorio = Math.floor( Math.random() * 893 );

  const caches = JSON.parse( localStorage.getItem( 'cache' ) || '[]' );

  if ( !caches.includes( numeroAleatorio ) ) {
    this.idBuscadoNaUltimaRequisicao = numeroAleatorio;

    caches.push( numeroAleatorio );

    localStorage.setItem( 'cache', JSON.stringify( caches ) );

    console.log( caches );

    // eslint-disable-next-line no-undef
    getInputValue( numeroAleatorio );
  }
}
