package br.com.dbccompany.cowork.Exceptions;

public class AcessoNaoEncontradoException extends Exception {

    private String mensagem;

    public AcessoNaoEncontradoException(String mensagem) {
        super("Acesso nao encontrado para " + mensagem);
        this.mensagem = mensagem;
    }
}
