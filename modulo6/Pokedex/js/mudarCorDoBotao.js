( function piscaBotaoAzul() {
  const s = document.getElementById( 'BotaoAzul' ).style;
  let f = false;
  const c1 = '#000080';
  const c2 = '#ffffff';

  setInterval( () => {
    s.backgroundColor = f ? c1 : c2;
    s.color = f ? c2 : c1;
    f = !f;
  }, 500 );
}() );
