import React, { Component } from 'react';
import BotaoUi from '../../componentes/botaoUi';
import { Checkbox } from 'antd';
import HeaderFormulario from '../../componentes/headerFormulario';
import Api from '../../models/api';
import InputArea from '../../componentes/inputFormulario';
import FooterPrincipal from '../../componentes/footer';

export default class FormularioCadastro extends Component {
  constructor(props) {
    super(props);
    this._api = new Api();
    this.state = { }
    this.salvarNome = this.salvarNome.bind(this);
    this.salvarLogin = this.salvarLogin.bind(this);
    this.salvaSenha = this.salvaSenha.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  salvarLogin(event) {
    this.setState({ login: event.target.value });
  }

  salvaSenha(event) {
    this.setState({ senha: event.target.value })
  }

  salvarNome(event) {
    this.setState({ nome: event.target.value });
  }

  handleSubmit(event) {
    this._api.registrarUsuario(this.state.nome, this.state.login, this.state.senha);
    event.preventDefault();
    window.location.reload();
  }

  render() {

    return (
      <React.Fragment>
        <HeaderFormulario link={'/login'} nome={'login'} />
        <div className='row formulario'>
          <div className='col col-sm-12 formulario-login'>
            <form className={'formulario-container'} onSubmit={this.handleSubmit}>
              <div>
                <h1 >crie sua conta no enjoei</h1>
                <InputArea nome={'nome completinho'} metodo={this.salvarNome} valor={this.state.nome} />
                <InputArea nome={'email'} metodo={this.salvarLogin} valor={this.state.login} />
                <InputArea nome={'senha super secreta'} metodo={this.salvaSenha} valor={this.state.senha} />
              </div>
              <div className='row formulario-row-divisor'>
                <div className='col col-sm-12 formulario-divisor' />
              </div>
              <div className='row' >
                <div className='col col-sm-12 formulario-checkbox' >
                  <Checkbox onChange={this.onChange}>aceito receber novidades do enjoei</Checkbox>
                </div>
              </div>
              <div className='row' >
                <div className='col col-sm-12 formulario-checkbox' >
                  <Checkbox onChange={this.onChange}>estou de acordo com os termos de serviço do enjoei</Checkbox>
                </div>
              </div>
              <input className={'botao-salmon  botao-formulario'} type="submit" value="criar conta" />
              <div className='row formulario-row-divisor'>
                <div className='col col-sm-5 formulario-divisor'>
                </div>
                <div className='col col-sm-2'>
                  <p className='formulario-divisor-palavra'>ou</p>
                </div>
                <div className='col col-sm-5 formulario-divisor'>
                </div>
              </div>
              <BotaoUi classe={'botao-formulario-facebook'} nome={'cadastre-se usando o facebook'} />
              <div className='row'>
                <span>
                  Protegido por reCAPTCHA - <span> Privacidade - Condições</span>
                </span>
              </div>
            </form>
          </div>
        </div>
        <FooterPrincipal />
      </React.Fragment>
    )
  };
}
