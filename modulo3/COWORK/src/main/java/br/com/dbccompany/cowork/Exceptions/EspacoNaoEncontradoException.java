package br.com.dbccompany.cowork.Exceptions;

public class EspacoNaoEncontradoException extends Exception {

    private String mensagem;

    public EspacoNaoEncontradoException(String mensagem) {
        super("Erro: espaco " + mensagem + "nao encontrado");
        this.mensagem = mensagem;
    }
}
