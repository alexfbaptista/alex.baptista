package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class TipoContatoRepositoryTestes {

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    private String nomeContato = "EMAIL";

    private TipoContatoEntity tipoContato;

    @BeforeEach
    public void setup() {
        tipoContato = new TipoContatoEntity(nomeContato);
        tipoContatoRepository.save(tipoContato);
    }

    @Test
    public void retornaEmail() {
        assertEquals(
                nomeContato,
                tipoContatoRepository.findByNome(nomeContato).getNome()
        );
    }

    @Test
    public void nullAoProcurarTipoNaoRegistrado() {
        assertNull(
                tipoContatoRepository.findByNome("TELEFONE")
        );
    }
}
