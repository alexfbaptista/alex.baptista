package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ContatoRepositoryTestes {

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    private ContatoEntity contato;

    private TipoContatoEntity tipoContato;

    private ClienteEntity cliente;


    @BeforeEach
    public void setup() {

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        tipoContato = tipoContatoRepository.save(
                new TipoContatoEntity("EMAIL")
        );

       contatoRepository.save(
                new ContatoEntity(tipoContato, cliente, "teste1@email.com")
        );

        contato = contatoRepository.save(
                new ContatoEntity(tipoContato, cliente, "teste2@email.com")
        );

        contatoRepository.save(
                new ContatoEntity(tipoContato, cliente, "teste3@email.com")
        );
    }

    @Test
    public void tamanhoDeList3ParaBuscarTodosPorTipoContato() {
        List<ContatoEntity> contatoEntities = contatoRepository.findAllByTipoContato(tipoContato);
        assertEquals(3, contatoEntities.size());
    }

    @Test
    public void tamanhoDeList3ParaBuscarTodosPorCliente() {
        List<ContatoEntity> contatoEntities = contatoRepository.findAllByCliente(cliente);
        assertEquals(3, contatoEntities.size());
    }

    @Test
    public void retornaContatoAoBuscarPorValor() {
        ContatoEntity contatoPersistido = contatoRepository.findByValor("teste2@email.com");
        assertEquals(contato, contatoPersistido);
    }
}
