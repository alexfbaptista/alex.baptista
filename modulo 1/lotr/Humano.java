public class Humano {
    private String nome;
    private Double vida = 75.0;

    public Humano() {
    }

    public Humano(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }
}
