import React from 'react';
import BotaoUi from '../botaoUi';

const BotoesHeader = () =>
    <React.Fragment>
        <div className='col col-sm-6 header-container-botoes'>
            <div className='row'>
                <div className='col col-sm-12 header-coluna-interna-botoes'>
                    <BotaoUi
                        classe={'botao botao-header'}
                        nome={'moças'}
                    />
                    <BotaoUi
                        classe={'botao botao-header'}
                        nome={'rapazes'}
                    />
                    <BotaoUi
                        classe={'botao botao-header'}
                        nome={'kids'}
                    />
                    <BotaoUi
                        classe={'botao botao-header'}
                        nome={'casa&tal'}
                    />
                    <BotaoUi
                        classe={'botao botao-header botao-duvida'}
                        nome={''}
                    ></BotaoUi>
                    <BotaoUi
                        classe={'botao botao-header letra-cinza'}
                        nome={'entrar'}
                        link={'login'}
                    />
                    <BotaoUi
                        classe={'botao botao-header botao-salmon'}
                        nome={'quero vender'}
                    />
                </div>
            </div>
        </div>
    </React.Fragment>

export default BotoesHeader;