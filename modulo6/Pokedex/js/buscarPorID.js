/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
function buscarPorID() {
  const id = document.getElementById( 'porID' ).value;

  if ( Number.isNaN( id ) ) {
    alert( 'Digite um id válido' );
  } else if ( this.idBuscadoNaUltimaRequisicao !== id ) {
    this.idBuscadoNaUltimaRequisicao = id;

    getInputValue( id );
  } else {
    alert( 'Id igual nao realiza nova requisicao' );
  }
}
