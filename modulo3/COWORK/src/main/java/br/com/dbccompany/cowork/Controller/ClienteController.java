package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.ClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Service.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/cliente/")
public class ClienteController {

    @Autowired
    private ClienteService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<ClienteDTO> todos() {
        logger.info("requisicao (GET) api/cliente/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja clientes registrados");

        return service.todosClientes();
    }

    @GetMapping(value = "/buscar/por-cpf/{cpf}")
    @ResponseBody
    public ClienteEntity porCpf(@PathVariable String cpf) {
        logger.info("requisicao (GET) api/cliente/buscar/porcpf/{cpf} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("cpf passado na requisicao: " + cpf);
        try {

            return Optional.ofNullable(
                    service.buscarPorCpf(cpf)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteEntity salvar(@RequestBody ClienteDTO dto) {
        logger.info("requisicao (POST) api/cliente/novo " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return service.salvar(dto);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClienteEntity editar(@PathVariable int id, @RequestBody ClienteDTO dto) {
        logger.info("requisicao (PUT) api/cliente/editar " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return service.editar(dto, id);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
