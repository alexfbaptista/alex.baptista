import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Mensagens from '../../constants/mensagens';

export default class MeuInputNumero extends Component {

  constructor(props) {
		super(props)
	}

  perdeFoco = evt => {
    const { obrigatorio, registrarNota } = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    registrarNota( { nota, erro } );
  }

  render(){
    const { className = '', placeholder, visivel, mensagem, deveExibirErro } = this.props;

    return visivel ? (
      <div className={ className }>
        { mensagem && <p>{ mensagem }</p> }
        <input type="number" className={ deveExibirErro ? 'erro' : '' } placeholder={ placeholder } onBlur={ this.perdeFoco } />
        { deveExibirErro && <span className="mensagem-erro" >{ Mensagens.ERRO.CAMPO_OBRIGATORIO }</span> }
      </div>
    ) : null;
  }
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool.isRequired,
  deveExibirErro: PropTypes.bool.isRequired,
  placeholder: PropTypes.string,
  mensagem: PropTypes.string,
  obrigatorio: PropTypes.bool
}

MeuInputNumero.defaultProps = {
  obrigatorio: false
}