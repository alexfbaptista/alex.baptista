import React from 'react';
import Rating from '@material-ui/lab/Rating';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import BotaoUi from '../botaoUi';

function formatadorData( data ) {
    let meses = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
    return `${meses[ new Date( data ).getMonth()]}/${new Date( data ).getFullYear()}`;
}

const DadosVendedor = ({ vendedor }) =>
    <React.Fragment>
        <div className='col col-sm-6 descricao-detalhes-vendedor-container'>
            <div className='row row-vendedor'>
                <div className='col col-sm-12'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        <div className='col col-sm-2'>
                        <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
                        </div>
                        <div className='col col-sm-6 informacoes-nome-vendedor'>
                            <span>{ vendedor.nome }</span>
                        </div>
                        <div className='col col-sm-4'>
                            <BotaoUi classe={ 'botao-seguir-vendedor-detalhes letra-salmao' } nome={ 'seguir' } />
                        </div>
                    </div>
                    <div className='row informacoes-nome-vendedor-estado-cidade'>
                        <span>{ `${ vendedor.estado } ${vendedor.cidade}` }</span>
                    </div>
                </div>
            </div>
            <div className='row row-vendedor'>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        avaliação
                    </div>
                    <div className='row'>
                        <Rating name="half-rating-read" defaultValue={ vendedor.avaliacao ? vendedor.avaliacao : 5 } precision={0.5} readOnly />
                    </div>
                </div>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        últimas entregas
                    </div>
                    <div className='row'>
                        <Rating name="half-rating-read" defaultValue={ vendedor.avaliacaoEntregas ? vendedor.avaliacaoEntregas : 5 } precision={0.5} readOnly />
                    </div>
                </div>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        tempo médio de envio
                    </div>
                    <div className='row'>
                        { vendedor.tempoMedio }
                    </div>
                </div>
            </div>
            <div className='row'>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        à venda
                    </div>
                    <div className='row'>
                        ---
                    </div>
                </div>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        vendidos
                    </div>
                    <div className='row'>
                        { vendedor.qtdItensVendidos }
                    </div>
                </div>
                <div className='col col-sm-4'>
                    <div className='row row-informacao-vendedor letra-cinza informacao-vendedor-cabecario'>
                        no enjoei desde
                    </div>
                    <div className='row'>
                        { formatadorData( vendedor.criacao ) }
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>

export default DadosVendedor;