package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClientePacoteEntity;
import br.com.dbccompany.cowork.Entity.ContratacaoEntity;
import br.com.dbccompany.cowork.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {

    PagamentoEntity findByClientePacote(ClientePacoteEntity clientePacote);
    PagamentoEntity findByContratacao(ContratacaoEntity contratacao);
}
