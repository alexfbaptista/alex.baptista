package br.com.dbccompany.cowork.DTO;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;

import javax.persistence.Column;
import java.sql.Date;
import java.time.LocalDate;

public class CadastroClienteDTO {

    @NotNull
    private String nome;

    @NotNull
    @Column(length = 11, columnDefinition = "CHAR", unique = true, nullable = false)
    private String cpf;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dataDeNascimento;

    @NotNull
    private String email;  //contato

    @NotNull
    private String telefone; //contato

    public ClienteEntity converterCliente() {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf);
        cliente.setDataNascimento(this.dataDeNascimento);
        return cliente;
    }

    public ContatoEntity converterEmail() {
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipo =  new TipoContatoEntity();
        tipo.setId(1);
        tipo.setNome("email");
        contato.setTipoContato(tipo);
        contato.setValor(this.email);
        return contato;
    }

    public ContatoEntity converterTelefone() {
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipo =  new TipoContatoEntity();
        tipo.setId(2);
        tipo.setNome("telefone");
        contato.setTipoContato(tipo);
        contato.setValor(this.telefone);
        return contato;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }
}
