import React from 'react';
import { Carousel } from 'antd';

const Banner = ({ banners }) =>
    <React.Fragment>
        <div className='row'>
            <div className='col col-sm-12'>
                <Carousel autoplay>
                    <div>
                        <img className='banner' src={banners[0].imagem} alt='banner' />
                    </div>
                    <div>
                        <img className='banner' src={banners[1].imagem} alt='banner' />
                    </div>
                    <div>
                        <img className='banner' src={banners[2].imagem} alt='banner' />
                    </div>
                    <div>
                        <img className='banner' src={banners[3].imagem} alt='banner' />
                    </div>
                </Carousel>
            </div>
        </div>
    </React.Fragment>

export default Banner;