import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../imagens/logo/logo.jpg'

const HeaderFormulario = ( { link, nome } ) =>
  <React.Fragment>
    <header className='header-formulario' >
      <Link className='header-formulario-texto' to={ link } > { nome } </Link>
      <span className='header-logo-formulario col-md-12'>
        <Link to='/'>
          <img className='logo-header-formulario' src={ logo } alt='logo' />
        </Link>
      </span>
    </header>
  </React.Fragment>

export default HeaderFormulario;
