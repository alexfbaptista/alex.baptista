/* eslint-disable no-unused-vars */
class Pokemon {
  constructor( objDaAPI ) {
    this._nome = objDaAPI.name;
    this._imagem = objDaAPI.sprites.front_default;
    this._altura = objDaAPI.height;
    this._id = objDaAPI.id;
    this._peso = objDaAPI.weight;
    this._tipos = objDaAPI.types;
    this._habilidades = objDaAPI.stats;
  }

  get nome() {
    return `Nome: ${ this._nome }`;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `Altura: ${ this._altura * 10 } cm`;
  }

  get id() {
    return `ID: ${ this._id }`;
  }

  get peso() {
    return `Peso: ${ Math.round( ( ( this._peso * 0.1 ) + Number.EPSILON ) * 100 ) / 100 } Kg`
  }

  get tipos() {
    const list = document.createElement( 'ul' );

    for ( let i = 0; i < this._tipos.length; i++ ) {
      const item = document.createElement( 'li' );
      item.appendChild( document.createTextNode( this._tipos[i].type.name.toUpperCase() ) );
      list.appendChild( item );
    }

    return list;
  }

  get status() {
    const list = document.createElement( 'ul' );

    for ( let i = 0; i < this._habilidades.length; i++ ) {
      const item = document.createElement( 'li' );

      const statusAtual = this._habilidades[i];

      item.appendChild(
        document.createTextNode(
          `${ statusAtual.stat.name.toUpperCase() }, Status-base: ${ statusAtual.base_stat }%, Effort: ${ statusAtual.effort }`,
        ),
      );
      list.appendChild( item );
    }

    return list;
  }
}
