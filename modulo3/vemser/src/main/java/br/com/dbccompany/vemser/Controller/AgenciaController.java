package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Service.AgenciaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/** URIs
 * {{url}}/api/agencia/todos
 * {{url}}/api/agencia/ver/{id}
 * {{url}}/api/agencia/novo
 * {{url}}/api/agencia/editar/{id}
 */

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController extends ControllerAbstract<AgenciaService, AgenciaEntity, Integer> {
}