package br.com.dbccompany.cowork.Utils.Datas;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;

public class CalculaDescontoEntradaSaida {

    private Duration minutosHorasDias;
    private Period mesAno;

    public CalculaDescontoEntradaSaida(LocalDateTime dataAcesso, LocalDateTime dataSaida) {

        minutosHorasDias = Duration.between(dataAcesso, dataSaida);

        mesAno = Period.between(dataAcesso.toLocalDate(), dataSaida.toLocalDate());
    }

    public int getMonths() {
        return Math.toIntExact(mesAno.getMonths());
    }

    public int getDays() {
        return Math.toIntExact(minutosHorasDias.toDays());
    }

    public int getHours() {
        return (minutosHorasDias.toHours() > 0)
                ? Math.toIntExact((long) Math.ceil(minutosHorasDias.toHours())) : 1;
    }

    public int getMinutes() {
        return (minutosHorasDias.toMinutes() > 0)
                ? Math.toIntExact((long) Math.ceil(minutosHorasDias.toMinutes())) : 1;
    }

    public int getTurnos() {
        return Math.toIntExact((long) Math.ceil(getHours() / 5D));
    }

    public int getSemanas() {
        return Math.toIntExact((long) Math.ceil(getDays() / 7));
    }
}
