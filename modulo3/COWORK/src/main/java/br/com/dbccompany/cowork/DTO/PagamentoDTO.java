package br.com.dbccompany.cowork.DTO;

import br.com.dbccompany.cowork.Entity.Enum.TipoPagamentoEnum;

import java.time.LocalDateTime;

public class PagamentoDTO {

    private String cpf;
    private String espacoNome;
    private int quantidade;
    private LocalDateTime vencimento;
    private int idClientePacote;
    private int idContratacao;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoDTO() {
    }

    public PagamentoDTO(String cpf, String espacoNome, int quantidade,
                        LocalDateTime vencimento, int idClientePacote,
                        int idContratacao, TipoPagamentoEnum tipoPagamento) {
        this.cpf = cpf;
        this.espacoNome = espacoNome;
        this.quantidade = quantidade;
        this.vencimento = vencimento;
        this.idClientePacote = idClientePacote;
        this.idContratacao = idContratacao;
        this.tipoPagamento = tipoPagamento;
    }

    public PagamentoDTO(String cpf, String espacoNome, int quantidade,
                        LocalDateTime vencimento, int idContratacao,
                        TipoPagamentoEnum tipoPagamento) {
        this.cpf = cpf;
        this.espacoNome = espacoNome;
        this.quantidade = quantidade;
        this.vencimento = vencimento;
        this.idContratacao = idContratacao;
        this.tipoPagamento = tipoPagamento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEspacoNome() {
        return espacoNome;
    }

    public void setEspacoNome(String espacoNome) {
        this.espacoNome = espacoNome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }

    public int getIdClientePacote() {
        return idClientePacote;
    }

    public void setIdClientePacote(int idClientePacote) {
        this.idClientePacote = idClientePacote;
    }

    public int getIdContratacao() {
        return idContratacao;
    }

    public void setIdContratacao(int idContratacao) {
        this.idContratacao = idContratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
