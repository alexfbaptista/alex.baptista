import React from 'react';
import BotaoUi from '../botaoUi';

const ProdutoDetalhadoImagem = ( { imagem='imagem' }) =>
    <React.Fragment>
        <div className='col col-sm-6 descricao-container-imagem'>
            <div className='row row-container-imagem'>
                <img className='col col-sm-10  descricao-imagem' src={ imagem } alt='logo' />
                <div className='col col-sm-2 botoes-descricao'>
                    <div className='row col col-sm-12'>
                        <BotaoUi classe={'botao-descricao-imagem botao-curtir'} nome={''} />
                    </div>
                    <div className='row col col-sm-12'>
                        <BotaoUi classe={'botao-descricao-imagem botao-comentarios'} nome={''} />
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>

export default ProdutoDetalhadoImagem;