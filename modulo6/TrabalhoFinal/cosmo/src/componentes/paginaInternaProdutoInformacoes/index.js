import React from 'react';
import BotaoUi from '../botaoUi';

const ProdutoDetalhadoInformacoes = ( { 
    valorBotao1='moças',
    valorBotao2='roupas',
    valorBotao3='casa',
    nome='Nome do Produto',
    valor='999.99',
    valorDescontado,
    parcelamentoSemJuros=' x parcelamentos',
    formasPagamento='master 💳 / visa 💳',
    tamanho='---',
    marca='---',
    condicao='---',
    codigo='---',
    descricao='---' 
    } ) =>
    
    <React.Fragment>
        <div className='col col-sm-6 descricao-opcoes-compra'>
            <div className='row descricao-compra-conjunto-botoes-primeiro'>
                <BotaoUi classe={'botao-descricao-compra-conjunto'} nome={ `${ valorBotao1 } /` } />
                <BotaoUi classe={'botao-descricao-compra-conjunto'} nome={ `${ valorBotao2 } /` } />
                <BotaoUi classe={'botao-descricao-compra-conjunto'} nome={ `${ valorBotao3 }` } />
            </div>
            <div className='row'>
                <h1 className='descricao-compra-nome-produto'>
                    { nome }
                </h1>
            </div>
            <div className='row descricao-compra-conjunto-botoes-segundo'>
                <BotaoUi classe={'botao-descricao-compra-conjunto'} nome={ `${ marca } /` } />
                <BotaoUi classe={'botao-descricao-compra-conjunto letra-salmao'} nome={'seguir marca'} />
            </div>
            <div className='row descricao-compra-valores'>
                <div className='row'>
                    <h3 className='letra-salmao' >
                        { `R$ ${ valor }` }
                    </h3>

                    {
                        valorDescontado ? (
                            <span className='letra-branca'>
                                {`° R$ ${ valorDescontado } na primeira compra`}
                            </span>
                        ) : null
                    }

                </div>
                <div className='row'>
                    <p className='letra-cinza'>
                        {`${ parcelamentoSemJuros } sem juros`}
                    </p>
                </div>
                <div className='row'>
                    <p className='letra-cinza'>
                        { formasPagamento }
                    </p>
                </div>
            </div>
            <div className='row'>
                <BotaoUi classe={'botao-descricao-comprar letra-branca'} nome={'eu quero'} />
            </div>
            <div className='row'>
                <BotaoUi classe={'botao-descricao-sacola letra-salmao'} nome={'adicionar à sacolinha'} />
            </div>
            <div className='row'>
                <BotaoUi classe={'botao-descricao-ofertar letra-salmao'} nome={'fazer oferta'} />
            </div>
            <div className='row descricao-detalhes-produto'>
                <div className='row row-superior-descricao-detalhes-produto'>
                    <div className='col col-sm-12'>
                        <span className='descricao-detalhes-produto-span'>
                            tamanho
                        </span>
                        <span>
                            { tamanho }
                        </span>
                    </div>
                </div>
                <div className='row row-inferior-descricao-detalhes-produto'>
                    <div className='col col-sm-4'>
                        <span className='descricao-detalhes-produto-span'>
                            marca
                        </span>
                        <span>
                            { marca }
                        </span>
                    </div>
                    <div className='col col-sm-4'>
                        <span className='descricao-detalhes-produto-span'>
                            condição
                        </span>
                        <span>
                            { condicao }
                        </span>
                    </div>
                    <div className='col col-sm-4'>
                        <span className='descricao-detalhes-produto-span'>
                            código
                        </span>
                        <span>
                            { `${ codigo }` }
                        </span>
                    </div>
                </div>
            </div>
            <div className='row descricao-detalhes-descricao letra-cinza'>
                <p>descricao</p>
            </div>
            <div className='row descricao-detalhes-descricao'>
                <span> { descricao } </span>
            </div>   
        </div>
    </React.Fragment>

export default ProdutoDetalhadoInformacoes;