package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.AcessoDTO;
import br.com.dbccompany.cowork.DTO.CPFClienteEspacoNomeDTO;
import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Repository.AcessoRepository;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.SaldoClienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class AcessoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    private ClienteEntity cliente;

    private EspacoEntity espaco;

    private AcessoDTO acessoDTO;

    private SaldoClienteEntity saldoCliente;

    private SaldoClienteId saldoClienteId;

    private ObjectMapper mapper;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTeste", "778899955", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 500, 350.0)
        );

        acessoDTO = new AcessoDTO(
                cliente.getCpf(),
                espaco.getNome(),
                LocalDateTime.now(),
                false
        );

        saldoClienteId = new SaldoClienteId(cliente.getId(), espaco.getId());

        saldoCliente = saldoClienteRepository.save(
                new SaldoClienteEntity(saldoClienteId, TipoContratacaoEnum.SEMANA, 999, LocalDateTime.now().plusMonths(5))
        );

        mapper = new ObjectMapper();

        acessoRepository.save(
                new AcessoEntity(saldoCliente, true, LocalDateTime.now(), false)
        );
    }

    @Test
    public void deveRetornar200QuandoConsultadoAcesso() throws Exception {

        URI uri = new URI("/api/acesso/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarAcessoEntrada() throws Exception {

        URI uri = new URI("/api/acesso/salvar/entrada");

        String json = "{\"cpf\": \"778899955\", \"nomeEspaco\":\"SalaTeste\", \"dataDeAcesso\": \"2020-10-13T12:45:13.836393\", " +
                "\"isExcecao\":\"false\"}";

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .exists()
                );
    }

    @Test
    public void salvarERetornarAcessoSaida() throws Exception {

        URI uri = new URI("/api/acesso/salvar/saida");

        String json = "{\"cpf\": \"778899955\", \"nomeEspaco\":\"SalaTeste\", \"dataDeAcesso\": \"2020-10-13T12:45:13.836393\", " +
                "\"isExcecao\":\"false\"}";

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .exists()
                );
    }

    @Test
    public void retornarAcessoPorClienteEspaco() throws Exception {

        URI uri = new URI("/api/acesso/buscar/por-cliente-espaco");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                new CPFClienteEspacoNomeDTO("778899955", "SalaTeste")
        );

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void buscarPorID() throws Exception {

        URI uri = new URI("/api/acesso/buscar/por-id/1");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .value(1)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.entrada")
                                .value(true)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.excecao")
                                .value(false)
                );
    }
}
