package br.com.dbccompany.cowork.DTO;

public class NomeEspacoDTO {

    private String nome;

    public NomeEspacoDTO() {
    }

    public NomeEspacoDTO(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
