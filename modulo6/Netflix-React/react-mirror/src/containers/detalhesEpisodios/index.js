import React, { Component } from 'react';
import EpisodioUi from '../../components/episodioUi';
import { NetflixHeader } from '../../components';

import { ListaEpisodios, EpisodiosApi } from '../../models';


import Mensagens from '../../constants/mensagens';

import {
  MensagemFlash,
} from '../../components';


export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.marcarComoAssistido = this.marcarComoAssistido.bind(this);
    this.state = {
      detalhes: null,
      deveExibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;

    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];
 
      Promise.all( requisicoes )
        .then( respostas => {
          console.log(respostas[1])
          let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
          this.setState( state => {
            return {
              ...state,
              episodio: listaEpisodios._todos.filter(e => e.id == episodioId)[0]
            }
          })
        })
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    this.setState(state => {
      return { ...state, episodio }
    })
  }

  registrarNota({ nota, erro }) {
    this.setState(state => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if (erro) {
      return;
    }
    const { episodio } = this.state;
    console.log(episodio)
    let corMensagem, mensagem;
    if (episodio.validarNota(nota)) {
      episodio.avaliar(nota).then(() => {
        corMensagem = 'verde';
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem({ corMensagem, mensagem });
      });
      window.location.reload();
    } else {
      corMensagem = 'vermelho';
      mensagem = Mensagens.ERRO.NOTA_INVALIDA;
      this.exibirMensagem({ corMensagem, mensagem });
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState(state => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState(state => {
      return {
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  render() {

    const { episodio, deveExibirErro, deveExibirMensagem, mensagem, corMensagem  } = this.state;

    return(
      <React.Fragment>
        <MensagemFlash
          atualizarMensagem={this.atualizarMensagem}
          mensagem={mensagem}
          cor={corMensagem}
          exibir={deveExibirMensagem}
          segundos="5" 
        />
        <NetflixHeader />
        {
          episodio ?
            <React.Fragment>
              <EpisodioUi 
                classeCard={ 'card-detalhes' } 
                episodio={ episodio } 
                detalhes={ episodio } 
                objNota={ episodio } 
                deveExibirErro={ deveExibirErro } 
                registrarNota={ this.registrarNota.bind(this) }
                />
              </React.Fragment> : null
        }
      </React.Fragment>
    )
  }
}