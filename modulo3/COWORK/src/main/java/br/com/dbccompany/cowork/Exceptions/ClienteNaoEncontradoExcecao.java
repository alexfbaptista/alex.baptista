package br.com.dbccompany.cowork.Exceptions;

public class ClienteNaoEncontradoExcecao extends Exception {

    private String mensagem;

    public ClienteNaoEncontradoExcecao(String mensagem) {
        super("Erro: cliente com cpf " + mensagem + "nao encontrado");
        this.mensagem = mensagem;
    }
}
