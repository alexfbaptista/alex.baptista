package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenciaRepository extends CrudRepository<AgenciaEntity, Integer> {

}