package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.DTO.ClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    ClienteEntity findByCpf(String cpf);
    ClienteEntity findByNome(String nome);
}
