
// Exercício 1
function calcularCirulo(raio, tipoCalculo) {
    switch(tipoCalculo) {
        case "A":
          return Math.PI * Math.pow(raio, 2);

        case "C":
          return 2 * Math.PI * raio;

        default:
          return "Valor errado inserido no tipo de calculo";
      }
}

console.log(calcularCirulo(10, "A"));
console.log(calcularCirulo(10, "C"));
console.log(calcularCirulo(10, "D"));

//Exercício 2

function naoBissexto(ano) {
  return !((ano % 400 == 0) || (ano % 4 == 0) && (ano % 100 != 0));
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));

//Exercício 3

function somarPares(numeros = []) {
  let somatorio = 0;
  for (let i = 0; i < numeros.length; i++) {
    if ( i % 2 == 0) {
        somatorio += numeros[i];
    }
  }
  return somatorio; 
}

console.log(somarPares( [ 1, 56, 4.34, 6, -2 ]));

//Exercício 4

function adicionar(entrada1) {
  return function(entrada2) {
      return entrada1 + entrada2;
  };
}

console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));

//Exercício 5

function imprimirBRL(numero) {
  return `R$ ${(Math.ceil((numero + Number.EPSILON) * 100) / 100).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
}

console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));