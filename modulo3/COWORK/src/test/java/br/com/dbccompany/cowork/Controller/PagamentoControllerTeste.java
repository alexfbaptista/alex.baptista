package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Entity.Enum.TipoPagamentoEnum;
import br.com.dbccompany.cowork.Repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDate;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class PagamentoControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    private ClienteEntity cliente;

    private EspacoEntity espaco;

    private ContratacaoEntity contratacao;

    private PacoteEntity pacote;

    private ClientePacoteEntity clientePacote;

    private PagamentoEntity pagamento;

    private String json;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        json = "{\"cpf\": \"44477788822\", \"espacoNome\":\"SalaTeste\", \"quantidade\": 20, " +
                "\"vencimento\":\"2035-01-01T00:00:00.332\", \"idContratacao\": 1, \"tipoPagamento\": \"CREDITO\"}";

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 40, 350.0)
        );

        contratacao = contratacaoRepository.save(
                new ContratacaoEntity(cliente, espaco, TipoContratacaoEnum.DIARIA,
                        10, 0.0, 4)
        );

        pacote = pacoteRepository.save(
                new PacoteEntity(800.0)
        );

        clientePacote = clientePacoteRepository.save(
                new ClientePacoteEntity(cliente, pacote, 40)
        );

        pagamento = pagamentoRepository.save(
                new PagamentoEntity(contratacao, TipoPagamentoEnum.CREDITO)
        );
    }

    @Test
    public void deveRetornar200QuandoBuscarTodosPagamentos() throws Exception {
        URI uri = new URI("/api/pagamento/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarPagamentoPorId() throws Exception {

        URI uri = new URI("/api/pagamento/buscar/por-id/1");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoPagamento")
                                .value("CREDITO")
                );
    }

    @Test
    public void retornarPagamentoPorContratacaoId() throws Exception {

        URI uri = new URI("/api/pagamento/buscar/por-id/contratacao/1");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoPagamento")
                                .value("CREDITO")
                );
    }

    @Test
    public void retornarPagamentoPorClientePacoteId() throws Exception {

        URI uri = new URI("/api/pagamento/buscar/por-id/cliente-pacote/1");

        pagamentoRepository.save(
                new PagamentoEntity(clientePacote, TipoPagamentoEnum.CREDITO)
        );

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id")
                                .exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoPagamento")
                                .value("CREDITO")
                );
    }

    @Test
    public void salvarERetornarPagamentoPorContratacao() throws Exception {

        URI uri = new URI("/api/pagamento/novo");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoPagamento")
                                .value("CREDITO")
                );
    }

    @Test
    public void salvarERetornarPagamentoPorPacote() throws Exception {

        json = "{\"cpf\": \"44477788822\", \"espacoNome\":\"SalaTeste\", \"quantidade\": 20, " +
                "\"vencimento\":\"2035-01-01T00:00:00.332\", \"idClientePacote\": 1, \"tipoPagamento\": \"CREDITO\"}";

        URI uri = new URI("/api/pagamento/novo");

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoPagamento")
                                .value("CREDITO")
                );
    }
}
