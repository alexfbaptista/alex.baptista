package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.DTO.ClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService extends ServiceAbstract<ClienteRepository, ClienteEntity, Integer> {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity salvar(ClienteDTO clienteDTO) {

        checaSeExisteTipoTelefoneEEmail();

        ClienteEntity cliente = super.salvar(
                new ClienteEntity(
                        clienteDTO.getNome(),
                        clienteDTO.getCpf(),
                        clienteDTO.getDataNascimento())
        );

        contatoService.salvar(new ContatoEntity(
                tipoContatoService.buscarPorTipo("email"),
                cliente,
                clienteDTO.getEmailValor()
        ));

        contatoService.salvar(new ContatoEntity(
                tipoContatoService.buscarPorTipo("telefone"),
                cliente,
                clienteDTO.getTelefoneValor()
        ));

        return cliente;
    }

    @Transactional
    public ClienteEntity editar(ClienteDTO clienteDTO, int id) {

        ClienteEntity cliente = new ClienteEntity(
                clienteDTO.getNome(),
                clienteDTO.getCpf(),
                clienteDTO.getDataNascimento()
        );

        cliente.setId(id);

        return repository.save(cliente);
    }

    public List<ClienteDTO> todosClientes() {

        List<ClienteEntity> clientes  = super.todos();

        List<ClienteDTO> clienteDTOS = new ArrayList<>();

        for (ClienteEntity cliente : clientes) {
            clienteDTOS.add(
                    new ClienteDTO(
                            cliente.getNome(),
                            cliente.getCpf(),
                            cliente.getDataNascimento()
                    )
            );
        }

        return clienteDTOS;
    }


    public ClienteEntity buscarPorCpf(String cpf) {
        return repository.findByCpf(cpf);
    }

    private void checaSeExisteTipoTelefoneEEmail() {

        Optional<TipoContatoEntity> email = Optional.ofNullable(tipoContatoService.buscarPorTipo("email"));

        Optional<TipoContatoEntity> telefone = Optional.ofNullable(tipoContatoService.buscarPorTipo("telefone"));

        if (!email.isPresent()) {

            tipoContatoService.salvar(new TipoContatoEntity("email"));

        }

        if (!telefone.isPresent()) {

            tipoContatoService.salvar(new TipoContatoEntity("telefone"));

        }
    }
}
