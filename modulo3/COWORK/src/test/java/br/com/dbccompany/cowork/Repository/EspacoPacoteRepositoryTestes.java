package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EspacoPacoteRepositoryTestes {

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    private EspacoPacoteEntity espacoPacote;

    private EspacoEntity espaco;

    private PacoteEntity pacote;

    @BeforeEach
    public void setup() {

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 40, 350.0)
        );

        pacote = pacoteRepository.save(
                new PacoteEntity(800.0)
        );

        espacoPacote = espacoPacoteRepository.save(
                new EspacoPacoteEntity(TipoContratacaoEnum.DIARIA, 10, 30,
                        espaco, pacote)
        );

        espacoPacote = espacoPacoteRepository.save(
                new EspacoPacoteEntity(TipoContratacaoEnum.DIARIA, 10, 30,
                        espaco, pacote)
        );

        espacoPacote = espacoPacoteRepository.save(
                new EspacoPacoteEntity(TipoContratacaoEnum.DIARIA, 10, 30,
                        espaco, pacote)
        );

        espacoPacote = espacoPacoteRepository.save(
                new EspacoPacoteEntity(TipoContratacaoEnum.DIARIA, 10, 30,
                        espaco, pacote)
        );
    }

    @Test
    public void tamanhoDeListPorPacote4() {
        List<EspacoPacoteEntity> espacoPacoteEntities = espacoPacoteRepository.findAllByPacote(pacote);
        assertEquals(4, espacoPacoteEntities.size());
    }

    @Test
    public void tamanhoDeListPorEspaco4() {
        List<EspacoPacoteEntity> espacoPacoteEntities = espacoPacoteRepository.findAllByEspaco(espaco);
        assertEquals(4, espacoPacoteEntities.size());
    }
}
