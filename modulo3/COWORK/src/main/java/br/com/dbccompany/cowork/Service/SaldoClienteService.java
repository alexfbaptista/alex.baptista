package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.DTO.SaldoClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.SaldoClienteEntity;
import br.com.dbccompany.cowork.Entity.SaldoClienteId;
import br.com.dbccompany.cowork.Exceptions.ClienteNaoEncontradoExcecao;
import br.com.dbccompany.cowork.Exceptions.EspacoNaoEncontradoException;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SaldoClienteService extends ServiceAbstract<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    public SaldoClienteEntity porId(SaldoClienteDTO dto) throws ClienteNaoEncontradoExcecao, EspacoNaoEncontradoException {

        Optional<ClienteEntity> cliente = Optional.ofNullable(clienteRepository.findByCpf(dto.getCpfCliente()));

        if (cliente.isPresent()) {

            Optional<EspacoEntity> espaco = Optional.ofNullable(espacoRepository.findByNome(dto.getEspacoNome()));

            if (espaco.isPresent()) {

                return repository.findById(new SaldoClienteId(
                        cliente.get().getId(),
                        espaco.get().getId()
                )).get();
            }

            throw new EspacoNaoEncontradoException(dto.getEspacoNome());
        }

        throw new ClienteNaoEncontradoExcecao(dto.getCpfCliente());
    }
}
