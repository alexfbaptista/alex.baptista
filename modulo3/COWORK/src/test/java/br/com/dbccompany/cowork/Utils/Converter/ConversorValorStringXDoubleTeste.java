package br.com.dbccompany.cowork.Utils.Converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConversorValorStringXDoubleTeste {

    private double valorSemFormatacao;
    private String valorFormatado;
    private ConversorValorStringXDouble conversor;

    @BeforeEach
    public void setup() {
        valorSemFormatacao = 500.0;
        valorFormatado = "R$ 750.57";
    }

    @Test
    public void pegaComFormatacaoERetornaSemFormacao() {
        conversor = new ConversorValorStringXDouble(valorFormatado);
        assertEquals(750.57, conversor.getValorEmDouble());
    }

    @Test
    public void pegaSemFormatacaoERetornaComFormatacao() {
        conversor = new ConversorValorStringXDouble(valorSemFormatacao);
        assertEquals("R$ 500.0", conversor.getValorFormatadoEmString());
    }
}
