import java.util.Random;

public class DadoD6 {

    private Random dice;

    {
        dice = new Random();
    }

    public int getDado() {
        return dice.nextInt(6) + 1;
    }
}