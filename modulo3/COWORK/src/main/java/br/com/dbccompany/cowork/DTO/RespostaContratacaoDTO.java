package br.com.dbccompany.cowork.DTO;

public class RespostaContratacaoDTO {

    double valor;

    public RespostaContratacaoDTO() {
    }

    public RespostaContratacaoDTO(double valor) {
        this.valor = valor;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
