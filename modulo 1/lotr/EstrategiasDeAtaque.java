import java.util.ArrayList;

public abstract class EstrategiasDeAtaque {

    protected ArrayList<Elfo> elfosAlistados;

    public EstrategiasDeAtaque(ExercitoDeElfos exercito) {
        this.elfosAlistados = exercito.getElfos();
    }

    abstract void atacar(Dwarf dwarf);

    abstract boolean composicao();

    protected boolean isElfoVivo(Elfo elfo) {
        return elfo.getVida() > 0;
    }

    private boolean temFlecha(Elfo elfo) {
        return elfo.getQtdFlechas() > 0;
    }

    private boolean isElfoVerde(Elfo elfo) {
        return elfo instanceof ElfoVerde;
    }

    private boolean isElfoNoturno(Elfo elfo) {
        return elfo instanceof ElfoNoturno;
    }

    public ArrayList<Integer> ordensElfosVerdes() {
        ArrayList<Integer> ints = new ArrayList<>();
        for (Elfo elfo : elfosAlistados) {
            if (isElfoVerde(elfo) && isElfoVivo(elfo) && temFlecha(elfo)) {
                ints.add(elfosAlistados.indexOf(elfo));
            }
        }
        return ints;
    }

    public ArrayList<Integer> ordensElfosNoturnos() {
        ArrayList<Integer> ints = new ArrayList<>();
        for (Elfo elfo : elfosAlistados) {
            if (isElfoNoturno(elfo) && isElfoVivo(elfo) && temFlecha(elfo)) {
                ints.add(elfosAlistados.indexOf(elfo));
            }
        }
        return ints;
    }
}
