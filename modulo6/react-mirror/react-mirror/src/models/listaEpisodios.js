import Episodio from "./episodio";

function _sortear ( min, max ) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor( Math.random() * (max - min) ) + min;
}

export default class ListaEpisodios {
	constructor( episodiosDoServidor = [], notasDoServidor = [] ){
		this._todos = episodiosDoServidor.map( elem => new Episodio( elem.id, elem.nome, elem.duracao, elem.temporada, elem.ordemEpisodio, elem.thumbUrl ) );
		this.atualizarNotas( notasDoServidor );
	}

	get avaliados() {
		return this._todos.filter( e => e.nota ).sort( ( a,b ) => a.temporada - b.temporada || a.ordemEpisodio - b.ordemEpisodio );
	}

	get episodiosAleatorios() {
		const indice = _sortear( 0, this._todos.length );
		return this._todos[ indice ];
	}

	atualizarNotas( notasDoServidor ) {
		this._todos = this._todos.map( episodio => {
			const nota = notasDoServidor.filter( n => n.episodioId === episodio.id ).sort( ( a,b ) => b.id - a.id )[0];
			episodio.nota = ( nota || {} ).nota;
			return episodio;
		} )
	}

}