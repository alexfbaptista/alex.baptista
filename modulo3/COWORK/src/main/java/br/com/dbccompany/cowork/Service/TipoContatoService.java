package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository, TipoContatoEntity, Integer> {
    public TipoContatoEntity buscarPorTipo(String tipo) {
        return repository.findByNome(tipo);
    }
}
