package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContaClienteRepository extends CrudRepository<ContaClienteEntity, ContaClienteId> {
    List<ContaClienteEntity> findAllByUsuario(UsuarioEntity usuario);
    List<ContaClienteEntity> findAllByBanco(int idBanco);
    List<ContaClienteEntity> findAllByConta(int idConta);
}
