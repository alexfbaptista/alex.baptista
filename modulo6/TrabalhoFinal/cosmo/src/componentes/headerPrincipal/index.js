import React, { Component } from 'react';
import BotoesHeader from '../botoesHeaderPrincipal/index.js';
import LogoInput from '../logoInputHeaderPrincipal';

export default class Header extends Component {
  buscarMetodoInput() {}
  render() {
    return (
      <React.Fragment>
        <header className='row-header'>
              <LogoInput metodo={ this.buscarMetodoInput } />
              <BotoesHeader />
        </header>
      </React.Fragment>
    )
  };
}
