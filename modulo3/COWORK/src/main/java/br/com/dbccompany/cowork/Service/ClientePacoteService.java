package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.Entity.ClientePacoteEntity;
import br.com.dbccompany.cowork.Repository.ClientePacoteRepository;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientePacoteService extends ServiceAbstract<ClientePacoteRepository, ClientePacoteEntity, Integer> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    public List<ClientePacoteEntity> todosPorCliente(String cpf) {

        return repository.findAllByCliente(
                clienteRepository.findByCpf(cpf)
        );
    }

    public List<ClientePacoteEntity> todosPorPacote(int idPacote) {

        return repository.findAllByPacote(
                pacoteRepository.findById(idPacote).get()
        );
    }
}
