package br.com.dbccompany.vemser.Entity;

public enum EstadoCivilEnum {
    SOLTEIRO, CASADO, DIVORCIADO, UNIAO_ESTAVEL, VIUVO
}
