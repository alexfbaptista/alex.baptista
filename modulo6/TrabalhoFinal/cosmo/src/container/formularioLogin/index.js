import React, { Component } from 'react';
import BotaoUi from '../../componentes/botaoUi';
import { Checkbox } from 'antd';
import HeaderFormulario from '../../componentes/headerFormulario';
import { Redirect } from 'react-router-dom';
import Api from '../../models/api';
import InputArea from '../../componentes/inputFormulario';
import FooterPrincipal from '../../componentes/footer';

export default class FormularioLogin extends Component {
  constructor(props) {
    super(props);
    this._api = new Api();
    this.state = {
      redirect: false
    }
    this.salvarLogin = this.salvarLogin.bind(this);
    this.salvaSenha = this.salvaSenha.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  salvarLogin(event) {
    this.setState({login: event.target.value});
    console.log(this.state.value)
  }

  salvaSenha(event) {
    this.setState({senha: event.target.value})
    console.log(this.state.senha)
  }

  handleSubmit(event) {
    this._api.usuarioPodeLogar( this.state.login, this.state.senha ).then(
      resposta => {
        if ( resposta.length > 0 ) {
          window.localStorage.setItem('token', true );
          this.setState({ redirect: true });
        }
      }
    )
    event.preventDefault();
  }

  render() {

    const { redirect } = this.state;

    if (redirect) {
      return <Redirect to='/'/>;
    }

    return (
      <React.Fragment>
          <HeaderFormulario link={ '/' } nome={ 'home' } />
          <div className='row formulario'>
              <div className='col col-sm-12 formulario-login'>
                <form className={ 'formulario-container' }  onSubmit={this.handleSubmit}>
                  <div className='row'>
                    <h1 >faça login no enjoei</h1>
                    <BotaoUi classe={ 'botao-formulario-facebook' } nome={ 'entre usando o facebook' } />
                    <div className='row formulario-row-divisor'>
                      <div className='col col-sm-5 formulario-divisor'>
                      </div>
                      <div className='col col-sm-2'>
                        <p className='formulario-divisor-palavra' >ou</p>
                      </div>
                      <div className='col col-sm-5 formulario-divisor'>
                      </div>
                    </div>
                    <InputArea nome={ 'email' } metodo={ this.salvarLogin } valor={ this.state.login } />
                    <InputArea nome={ 'senha' } metodo={ this.salvaSenha } valor={ this.state.senha } />
                  </div>
                  <div className='row' >
                    <div className='col col-sm-6 formulario-checkbox' >
                      <Checkbox onChange={ this.onChange }>continuar conectado</Checkbox>
                    </div>
                    <div className='col col-sm-6' >
                      <BotaoUi classe={ 'botao-formulario-esqueci-senha' } nome={ 'esqueci a senha' } />
                    </div>
                  </div>
                  <input className={ 'botao-salmon  botao-formulario' } type="submit" value="Logar" />
                  <BotaoUi 
                    classe={ 'botao-formulario-navegar-registro' } 
                    nome={ 'não tenho conta' } 
                    link={ '/cadastro' } 
                  />
                  <div className='row'>
                    <span>
                      Protegido por reCAPTCHA - <span> Privacidade - Condições</span>
                    </span>
                  </div>
                </form>
              </div>
          </div>
          <FooterPrincipal />
      </React.Fragment>
    )
  };
}
