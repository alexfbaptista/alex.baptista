import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NoturnosPorUltimoTeste {

    ElfoVerde ev1;
    ElfoVerde ev2;
    ElfoVerde ev3;
    ElfoVerde ev4;
    ElfoNoturno en1;
    ElfoNoturno en2;
    ElfoNoturno en3;
    ElfoNoturno en4;
    ExercitoDeElfos exercito;
    NoturnosPorUltimo estrategia;

    @Test
    public void apenasElfosVerdesNas4PrimeirasPosicoes() {
        assertEquals(ev1, estrategia.pelotaoOrdenado().get(0));
        assertEquals(ev3, estrategia.pelotaoOrdenado().get(1));
        assertEquals(ev4, estrategia.pelotaoOrdenado().get(2));
        assertEquals(ev2, estrategia.pelotaoOrdenado().get(3));
    }

    @Test
    public void apenasElfosNoturnosNas4PrimeirasPosicoes() {
        assertEquals(en1, estrategia.pelotaoOrdenado().get(4));
        assertEquals(en2, estrategia.pelotaoOrdenado().get(5));
        assertEquals(en3, estrategia.pelotaoOrdenado().get(6));
        assertEquals(en4, estrategia.pelotaoOrdenado().get(7));
    }

    @Before
    public void setup() {

        ev1 = new ElfoVerde("ev1");
        ev2 = new ElfoVerde("ev2");
        ev3 = new ElfoVerde("ev3");
        ev4 = new ElfoVerde("ev4");
        en1 = new ElfoNoturno("en1");
        en2 = new ElfoNoturno("en1");
        en3 = new ElfoNoturno("en1");
        en4 = new ElfoNoturno("en1");

        exercito = new ExercitoDeElfos();

        exercito.alistar(ev1);
        exercito.alistar(ev3);
        exercito.alistar(en1);
        exercito.alistar(ev4);
        exercito.alistar(en2);
        exercito.alistar(en3);
        exercito.alistar(en4);
        exercito.alistar(ev2);

        estrategia = new NoturnosPorUltimo(exercito);
    }
}
