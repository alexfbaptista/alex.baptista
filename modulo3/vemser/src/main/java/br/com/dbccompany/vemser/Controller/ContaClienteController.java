package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ContaClienteEntity;
import br.com.dbccompany.vemser.Entity.ContaClienteId;
import br.com.dbccompany.vemser.Service.ContaClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/contacliente/todos
 * {{url}}/api/contacliente/ver/{id}
 * {{url}}/api/contacliente/novo
 * {{url}}/api/contacliente/editar/{id}
 */

@Controller
@RequestMapping("api/contacliente/")
public class ContaClienteController extends ControllerAbstract<ContaClienteService, ContaClienteEntity, ContaClienteId>{
}
