package br.com.dbccompany.logs.Service;

import br.com.dbccompany.logs.Entity.Logs;
import br.com.dbccompany.logs.Repository.LogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class LogsService {

    @Autowired
    private LogsRepository logsRepository;

    @Transactional( rollbackFor = Exception.class )
    public List<Logs> findAll() {
        return logsRepository.findAll();
    }

    public Logs findById(@PathVariable String id) {
        return logsRepository.findById(id).get();
    }

    @Transactional( rollbackFor = Exception.class )
    public Logs save(@RequestBody String data) {
        return logsRepository.save(new Logs(data));
    }
}
