package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;
import br.com.dbccompany.vemser.Repository.MovimentacoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class MovimentacoesService extends ServiceAbstract<MovimentacoesRepository, MovimentacaoEntity, Integer> {

    public List<MovimentacaoEntity> buscarTodosPorConta(int idConta) {
        return repository.findAllByConta(idConta);
    }

    public List<MovimentacaoEntity> buscarTodosPorTipoMovimentacao(String tipoMovimentacao) {
        return repository.findAllByTipoMovimentacao(tipoMovimentacao);
    }
}
