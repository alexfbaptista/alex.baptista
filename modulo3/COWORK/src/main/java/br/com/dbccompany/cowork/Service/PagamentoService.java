package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.PagamentoDTO;
import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Exceptions.ContratacaoEPacoteAusentesNoDTOException;
import br.com.dbccompany.cowork.Repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Transactional(rollbackFor = Exception.class)
    public PagamentoEntity salvar(PagamentoDTO pagamentoDTO) throws Exception {

        if (isContratacao(pagamentoDTO)) {

            return realizaPagamentoPorContratacao(pagamentoDTO);

        } else if (isPacote(pagamentoDTO)) {

            return realizaPagamentoPorPacote(pagamentoDTO);

        } else {

            logger.error("Contratacao e Pacote ausentes");

            throw new ContratacaoEPacoteAusentesNoDTOException();
        }
    }

    public PagamentoEntity realizaPagamentoPorPacote(PagamentoDTO pagamentoDTO) {
        logger.info("Pagamento com pacote");

        Optional<ClientePacoteEntity> clientePacote = clientePacoteRepository.findById(pagamentoDTO.getIdClientePacote());

        if (clientePacote.isPresent()) {

            logger.info("Cliente de cpf " + clientePacote.get().getCliente().getCpf() + " encontrado");
            logger.info("Pacote de id " + clientePacote.get().getPacote().getId() + " encontrado");
            logger.info("Valor do pacote " + clientePacote.get().getPacote().getValor());

            Optional<PacoteEntity> pacote = pacoteRepository.findById(
                    clientePacote.get().getPacote().getId()
            );

            List<EspacoPacoteEntity> espacosPacoteList = espacoPacoteRepository
                    .findAllByPacote(pacote.get());

            logger.info("Total de espacos pacotes encontrados: " + espacosPacoteList.size());

            for (EspacoPacoteEntity espacosPacotes: espacosPacoteList) {

                SaldoClienteId saldoClienteId = getSaldoClienteId(pagamentoDTO, espacosPacotes);

                saldoClienteRepository.save(
                        new SaldoClienteEntity(
                                saldoClienteId,
                                espacosPacotes.getTipoContratacao(),
                                espacosPacotes.getQuantidade() + pagamentoDTO.getQuantidade(),
                                pagamentoDTO.getVencimento()
                        )
                );

                espacosPacotes.setQuantidade(espacosPacotes.getQuantidade() + pagamentoDTO.getQuantidade());

                espacoPacoteRepository.save(espacosPacotes);
            }

            PagamentoEntity pagamento = pagamentoRepository.save(
                    new PagamentoEntity(
                        clientePacote.get(),
                        pagamentoDTO.getTipoPagamento()
            ));

            logger.info("Pagamento realizado com sucesso");

            return pagamento;
        }

        logger.error("Elemento de id: " + pagamentoDTO.getIdClientePacote() + " nao encontrado na tabela cliente pacote");
        throw new NoSuchElementException();
    }

    public PagamentoEntity realizaPagamentoPorContratacao(PagamentoDTO pagamentoDTO) {
        logger.info("Pagamento com contratacao");

        Optional<ContratacaoEntity> contratacao = contratacaoRepository.findById(pagamentoDTO.getIdContratacao());

        if (contratacao.isPresent()) {

            SaldoClienteId saldoClienteId = new SaldoClienteId(
                    clienteRepository.findByCpf(pagamentoDTO.getCpf()).getId(),
                    espacoRepository.findByNome(pagamentoDTO.getEspacoNome()).getId()
            );

            Optional<SaldoClienteEntity> saldoCliente = saldoClienteRepository.findById(saldoClienteId);

            if (saldoCliente.isPresent()) {

                atualizaQuantidadeSaldo(pagamentoDTO, saldoCliente);

                saldoClienteRepository.save(saldoCliente.get());

                logger.info("Saldo do cliente foi atualizado");

            } else {

                saldoClienteRepository.save(new SaldoClienteEntity(
                        saldoClienteId,
                        contratacao.get().getTipoContratacao(),
                        pagamentoDTO.getQuantidade(),
                        pagamentoDTO.getVencimento()
                ));

                logger.info("Novo saldo criado para o cliente");
            }

            PagamentoEntity pagamento = pagamentoRepository.save(new PagamentoEntity(
                    contratacao.get(),
                    pagamentoDTO.getTipoPagamento()
            ));

            logger.info("Pagamento realizado com sucesso");

            return pagamento;
        }

        logger.error("Elemento de id: " + pagamentoDTO.getIdContratacao() + " nao encontrado na tabela contratacao");

        throw new NoSuchElementException();
    }

    public SaldoClienteId getSaldoClienteId(PagamentoDTO pagamentoDTO, EspacoPacoteEntity espacosPacotes) {
        return new SaldoClienteId(
                clienteRepository.findByCpf(pagamentoDTO.getCpf()).getId(),
                espacoRepository.findByNome(espacosPacotes.getEspaco().getNome()).getId()
        );
    }

    public void atualizaQuantidadeSaldo(PagamentoDTO pagamentoDTO, Optional<SaldoClienteEntity> saldoCliente) {
        saldoCliente.get().setQuantidade(
                saldoCliente.get().getQuantidade() + pagamentoDTO.getQuantidade()
        );
    }

    public boolean isContratacao(PagamentoDTO pagamentoDTO) {
        Integer id = pagamentoDTO.getIdContratacao();
        return verificaIds(id);
    }

    private boolean verificaIds(Integer id) {
        return id != null && id != 0;
    }

    public boolean isPacote(PagamentoDTO pagamentoDTO) {
        Integer id = pagamentoDTO.getIdClientePacote();
        return verificaIds(id);
    }

    public List<PagamentoEntity> todos() {
        return (List<PagamentoEntity>) pagamentoRepository.findAll();
    }

    public PagamentoEntity porId(int id) {
        return pagamentoRepository.findById(id).get();
    }

    public PagamentoEntity buscarPorClientePacote(int id) {
        return pagamentoRepository
                .findByClientePacote(
                        clientePacoteRepository.findById(id).get()
                );
    }

    public PagamentoEntity buscarPorContratacao(int id) {
        return pagamentoRepository
                .findByContratacao(
                        contratacaoRepository.findById(id).get()
                );
    }
}
