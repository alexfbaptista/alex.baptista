package br.com.dbccompany.cowork.Exceptions;

public class SaldoClienteNaoEncontradoException extends Exception {

    private String mensagem;

    public SaldoClienteNaoEncontradoException(String mensagem) {
        super("Erro: saldo e cliente nao encontrado para" + mensagem);
        this.mensagem = mensagem;
    }
}
