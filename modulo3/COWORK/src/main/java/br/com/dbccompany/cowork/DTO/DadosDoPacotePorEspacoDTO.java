package br.com.dbccompany.cowork.DTO;

import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;

public class DadosDoPacotePorEspacoDTO {

    private String nomeEspaco;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int prazo;

    public DadosDoPacotePorEspacoDTO() {
    }

    public DadosDoPacotePorEspacoDTO(String nomeEspaco, TipoContratacaoEnum tipoContratacao, int quantidade, int prazo) {
        this.nomeEspaco = nomeEspaco;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.prazo = prazo;
    }

    public String getNomeEspaco() {
        return nomeEspaco;
    }

    public void setNomeEspaco(String nomeEspaco) {
        this.nomeEspaco = nomeEspaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
