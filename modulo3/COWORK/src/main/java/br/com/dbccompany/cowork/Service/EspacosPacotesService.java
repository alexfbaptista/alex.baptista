package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Repository.EspacoPacoteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacosPacotesService extends ServiceAbstract<EspacoPacoteRepository, EspacoPacoteEntity, Integer> {

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    public List<EspacoPacoteEntity> todosPorEspaco(String espacoNome) {
        return repository
                .findAllByEspaco(
                        espacoRepository.findByNome(espacoNome)
                );
    }

    public List<EspacoPacoteEntity> todosPorPacote(int idPacote) {
        return repository
                .findAllByPacote(
                        pacoteRepository.findById(idPacote).get()
                );
    }
}
