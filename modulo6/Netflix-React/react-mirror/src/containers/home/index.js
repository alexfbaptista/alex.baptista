import React, { Component } from 'react';

import './home.css';

import { ListaEpisodiosUi, NetflixHeader } from '../../components';

import { ListaEpisodios, EpisodiosApi } from '../../models';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: []
    }
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];
    Promise.all(requisicoes)
      .then(respostas => {
        let listaEpisodios = new ListaEpisodios(respostas[0], respostas[2], respostas[1]);
        this.setState(state => {
          return {
            ...state,
            listaEpisodios: listaEpisodios._todos.concat([])
          }
        })
      })
    }

  componentWillReceiveProps() {
    this.componentDidMount();
  }

  filtrarPorTermo = evt => {
    if ( evt.key === 'Enter' ) {
    const termo = evt.target.value;
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas(),
      this.episodiosApi.filtrarPorTermo(termo)
    ];
    Promise.all(requisicoes)
    .then(respostas => {
      console.log(respostas[1])
      let listaEpisodios = new ListaEpisodios(respostas[0], respostas[2], respostas[1]);
      this.setState(state => {
        return {
          ...state,
          listaEpisodios: listaEpisodios._todos.concat([]).filter(e => respostas[3].some(r => r.episodioId === e.id))
        }
      })
    })
    }
  }

  episodioAleatorio() {
    const { listaEpisodios } = this.state;
    let indexAleatorio = Math.floor(Math.random() * (listaEpisodios.length - 1)) + 1;
    let url = `episodio/${indexAleatorio}`;
    this.props.history.push(url);
  }

  render() {

    const { listaEpisodios } = this.state;

    return (
      <React.Fragment>
        <NetflixHeader filtrarPorTermo={this.filtrarPorTermo} episodioAleatorio={this.episodioAleatorio.bind(this)} />
        <div>
          <ListaEpisodiosUi listaEpisodios={listaEpisodios} />
        </div>
      </React.Fragment>
    )
  };
}
