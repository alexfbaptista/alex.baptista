import java.util.ArrayList;
import java.util.Comparator;

public class MaioriaDeElfosVerdes extends EstrategiasDeAtaque {

    public MaioriaDeElfosVerdes(ExercitoDeElfos exercito) {
        super(exercito);
    }

    @Override
    void atacar(Dwarf dwarf) {
        if (composicao()) {
            for (Elfo elfo : pelotaoApto()) {
                elfo.atacar(dwarf);
            }
        }
    }

    public ArrayList<Elfo> ordenadorPorQtdDeFlechas(ArrayList<Elfo> pelotao) {
        pelotao.sort(Comparator.comparing(Elfo::getQtdFlechas));
        return pelotao;
    }

    public ArrayList<Elfo> pelotaoApto() {
        ArrayList<Elfo> pelotao = new ArrayList<>();
        for (int i = 0; i < ordensElfosNoturnos().size(); i++) {
            pelotao.add(elfosAlistados.get(ordensElfosNoturnos().get(i)));
        }
        for (int i = 0; i < ordensElfosVerdes().size();i++) {
            pelotao.add(elfosAlistados.get(ordensElfosVerdes().get(i)));
        }
        return ordenadorPorQtdDeFlechas(pelotao);
    }

    @Override
    public boolean composicao() {
        return Double.valueOf(ordensElfosNoturnos().size()) <= Double.valueOf(ordensElfosVerdes().size() * 0.3);
    }
}
