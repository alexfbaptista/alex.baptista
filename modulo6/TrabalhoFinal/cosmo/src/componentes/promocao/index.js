import React from 'react';

const MensagemPromocao = ( { mensagem, cor, coluna  } ) => (
    <React.Fragment>
        <div className='row'>
          <span className={ `promocao ${ cor } ${ coluna } ` }>
              <p>
                  { mensagem }
              </p>
          </span>
        </div>
    </React.Fragment> 
  )

export default MensagemPromocao;