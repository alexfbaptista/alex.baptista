import React from 'react';
import ProdutoDetalhadoImagem from '../paginaInternaProdutoImagem';
import ProdutoDetalhadoInformacoes from '../paginaInternaProdutoInformacoes';

const ProdutoDetalhado = ({ produto, detalhes }) =>
            <React.Fragment>
                <div className='row descricao-container'>
                    <ProdutoDetalhadoImagem imagem={ produto.imagem } />
                    <ProdutoDetalhadoInformacoes
                        nome={ produto.nome }
                        marca={ detalhes.marca }
                        condicao={ detalhes.condicao }
                        codigo={ detalhes.codigo }
                        tamanho={ detalhes.tamanho }
                        descricao={ detalhes.descricao }
                        valor={ produto.preco }
                        valorDescontado={ produto.preco * 0.9 }
                        parcelamentoSemJuros={ `10x R$ ${ produto.preco / 10 } sem juros` }
                    />
                </div>
            </React.Fragment>

export default ProdutoDetalhado;
