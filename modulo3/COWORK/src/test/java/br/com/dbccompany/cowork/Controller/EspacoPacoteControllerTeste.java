package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.NomeEspacoDTO;
import br.com.dbccompany.cowork.DTO.PacoteDTO;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.EspacoPacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Repository.EspacoPacoteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.PacoteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class EspacoPacoteControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacoPacoteRepository espacoPacoteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private EspacoEntity espaco;

    private PacoteDTO pacoteDTO;

    private PacoteEntity pacote;

    private EspacoPacoteEntity espacoPacote;

    private ObjectMapper mapper;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 10, 150.0)
        );

        pacote = pacoteRepository.save(
                new PacoteEntity(950.0)
        );

        mapper = new ObjectMapper();

        espacoPacote = espacoPacoteRepository.save(
                new EspacoPacoteEntity(TipoContratacaoEnum.SEMANA, 40, 20, espaco, pacote)
        );
    }

    @Test
    public void deveRetornar200QuandoBuscarTodosEspacoPacote() throws Exception {
        URI uri = new URI("/api/espaco-pacote/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarUmEspacoPacotePorId() throws Exception {

        URI uri = new URI("/api/espaco-pacote/buscar/por-id/1");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoContratacao")
                                .value("SEMANA")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.quantidade")
                                .value(40)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.prazo")
                                .value(20)
                );
    }

    @Test
    public void retornarUmEspacoPacotePorIdDoPacote() throws Exception {

        URI uri = new URI("/api/espaco-pacote/buscar/por-id-pacote/1");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarUmEspacoPacotePorEspaco() throws Exception {

        URI uri = new URI("/api/espaco-pacote/buscar/por-espaco");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                new NomeEspacoDTO("SalaTeste")
        );

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }
}
