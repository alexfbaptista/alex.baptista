package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    List<ContatoEntity> findAllByTipoContato(TipoContatoEntity tipoContato);
    List<ContatoEntity> findAllByCliente(ClienteEntity cliente);
    ContatoEntity findByValor(String valor);
}
