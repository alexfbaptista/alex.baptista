package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class ControllerAbstract<
        S extends ServiceAbstract,
        E extends EntityAbstract,
        I> {

    @Autowired
    protected S service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<E> todos() {
        return service.todos();
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public E porId(@PathVariable I id, @RequestBody E entidade) {
        return (E) service.porId(id);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public E salvar(@RequestBody E entidade) {
        return (E) service.salvar(entidade);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable I id, @RequestBody E entidade) {
        entidade.setId(id);
        return (E) service.salvar(entidade);
    }



}
