package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    TipoContatoEntity findByNome(String nome);
}
