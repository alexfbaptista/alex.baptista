package br.com.dbccompany.cowork.DTO;

public class CPFClienteEspacoNomeDTO {

    private String cpfCliente;
    private String espacoNome;

    public CPFClienteEspacoNomeDTO() {
    }

    public CPFClienteEspacoNomeDTO(String cpfCliente, String espacoNome) {
        this.cpfCliente = cpfCliente;
        this.espacoNome = espacoNome;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getEspacoNome() {
        return espacoNome;
    }

    public void setEspacoNome(String espacoNome) {
        this.espacoNome = espacoNome;
    }
}
