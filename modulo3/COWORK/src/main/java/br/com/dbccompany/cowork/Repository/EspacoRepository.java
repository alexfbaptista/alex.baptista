package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    EspacoEntity findByNome(String nome);
    EspacoEntity findByQtdPessoas(int quantidade);
    List<EspacoEntity> findByQtdPessoasGreaterThanEqual(int quantidade);
}
