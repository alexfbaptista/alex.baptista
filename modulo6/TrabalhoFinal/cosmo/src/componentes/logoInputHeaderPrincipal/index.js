import React from 'react';
import logo from '../../imagens/logo/logo.jpg';
import { Input } from 'antd';
import { Link } from 'react-router-dom';
const { Search } = Input;

const LogoInput = ( { metodo } ) =>
    <React.Fragment>
        <div className='col col-sm-6 header-container-logo-input'>
            <div className='col col-sm-8'>
                <span className='header-logo col-md-12'>
                    <Link to='/'>
                        <img className='logo-header' src={ logo } alt='logo' />
                    </Link>
                </span>
            </div>
            <div className='col col-sm-4'>
                <Search className="input-header" placeholder={`busque "laco"`} onSearch={ metodo } size={ 'large' } />
            </div>
        </div>
    </React.Fragment>

export default LogoInput;