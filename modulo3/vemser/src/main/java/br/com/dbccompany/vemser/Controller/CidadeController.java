package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Service.CidadeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/cidade/todos
 * {{url}}/api/cidade/ver/{id}
 * {{url}}/api/cidade/novo
 * {{url}}/api/cidade/editar/{id}
 */

@Controller
@RequestMapping("/api/cidade")
public class CidadeController extends ControllerAbstract<CidadeService, CidadeEntity, Integer>{
}
