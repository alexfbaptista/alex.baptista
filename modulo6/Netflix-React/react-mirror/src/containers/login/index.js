import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { BotaoUi } from '../../components';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };

    this.salvarLogin = this.salvarLogin.bind(this);
    this.salvaSenha = this.salvaSenha.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  salvarLogin(event) {
    this.setState({login: event.target.value});
    console.log(this.state.value)
  }

  salvaSenha(event) {
    this.setState({senha: event.target.value})
    console.log(this.state.senha)
  }

  handleSubmit(event) {
    let senhaSalva = window.localStorage.getItem(this.state.login);
    if( senhaSalva != null) {
      if( this.state.senha === senhaSalva) {
        window.localStorage.setItem('token', true );
        this.setState({ redirect: true });
      }
    }
    event.preventDefault();
  }

  render() {

    const { redirect } = this.state;

     if (redirect) {
       return <Redirect to='/home'/>;
     }

    return (
      <React.Fragment>
        <div className='card-login-cadastro' >
          <form onSubmit={this.handleSubmit}>
            <div>
              <h1 className="titulo-cadastro-login" >Realizar Login</h1>
              <label>
                <input type="text" value={this.state.login} onChange={this.salvarLogin} placeholder='Login...' />
              </label>
              <label>
                <input type="text" value={this.state.senha} onChange={this.salvaSenha} placeholder='Senha...' />
              </label>
            </div>
            <input className="botao login-cadastro" type="submit" value="Logar" />
            <BotaoUi link='/cadastro' nome="Novo Usuario" />
          </form>
          
        </div>
      </React.Fragment>
    );
  }
}
