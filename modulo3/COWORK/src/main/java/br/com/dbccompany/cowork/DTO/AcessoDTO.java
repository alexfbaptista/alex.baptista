package br.com.dbccompany.cowork.DTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class AcessoDTO {

    private String cpf;
    private String nomeEspaco;
    private LocalDateTime dataDeAcesso;
    private boolean isExcecao;

    public AcessoDTO() {
    }

    public AcessoDTO(String cpf, String nomeEspaco, LocalDateTime dataDeAcesso, boolean isExcecao) {
        this.cpf = cpf;
        this.nomeEspaco = nomeEspaco;
        this.dataDeAcesso = dataDeAcesso;
        this.isExcecao = isExcecao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNomeEspaco() {
        return nomeEspaco;
    }

    public void setNomeEspaco(String nomeEspaco) {
        this.nomeEspaco = nomeEspaco;
    }

    public LocalDateTime getDataDeAcesso() {
        return dataDeAcesso;
    }

    public void setDataDeAcesso(LocalDateTime dataDeAcesso) {
        this.dataDeAcesso = dataDeAcesso;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
