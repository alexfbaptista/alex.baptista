package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.AcessoDTO;
import br.com.dbccompany.cowork.DTO.CPFClienteEspacoNomeDTO;
import br.com.dbccompany.cowork.Entity.AcessoEntity;
import br.com.dbccompany.cowork.Service.AcessoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/acesso/")
public class AcessoController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    private AcessoService acessoService;

    @PostMapping(value = "/salvar/entrada")
    @ResponseBody
    public AcessoEntity salvarEntrada(@RequestBody AcessoDTO acessoDTO) {
        logger.info("requisicao (POST) api/acesso/salvar/entrada " +
                "sendo processada, data: " + LocalDate.now());
        try {

            return acessoService.salvarEntrada(acessoDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "/salvar/saida")
    @ResponseBody
    public AcessoEntity salvarSaida(@RequestBody AcessoDTO acessoDTO) {
        logger.info("requisicao (POST) api/acesso/salvar/saida " +
                "sendo processada, data: " + LocalDate.now());
        try {


            return acessoService.salvarSaida(acessoDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<AcessoEntity> todos() {
        logger.info("requisicao (GET) api/acesso/buscar/todos " +
                "sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja acessos registrados");

        return acessoService.todos();
    }

    //Verificar com o Marcos a necessidade de existir a chamada para editar em acesso
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public AcessoEntity editar(@PathVariable int id, @RequestBody AcessoEntity acessoEntity) {
        logger.info("requisicao (PUT) api/acesso/editar sendo processada, data: " + LocalDate.now());
        try {

            acessoEntity.setId(id);
            return acessoService.salvar(acessoEntity);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public AcessoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/acesso/buscar/por-id/{id} " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    acessoService.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/buscar/por-cliente-espaco")
    @ResponseBody
    public List<AcessoEntity> porSaldoCliente(@RequestBody CPFClienteEspacoNomeDTO cpfClienteEspacoNomeDTO) {
        logger.info("requisicao (GET) api/acesso/buscar/por-cliente-espaco " +
                "sendo processada, data: " + LocalDate.now());
        logger.info("CPF passado na requisicao: " + cpfClienteEspacoNomeDTO.getCpfCliente());
        logger.info("Nome passado na requisicao: " + cpfClienteEspacoNomeDTO.getEspacoNome());

        try {

            return acessoService.buscarAcessosPorSaldoCliente(cpfClienteEspacoNomeDTO);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
