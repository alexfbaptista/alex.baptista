package br.com.dbccompany.cowork.Entity.Enum;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
