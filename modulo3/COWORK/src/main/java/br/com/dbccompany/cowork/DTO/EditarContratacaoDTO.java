package br.com.dbccompany.cowork.DTO;

import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;

public class EditarContratacaoDTO {

    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private int desconto;
    private int prazo;

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }
}
