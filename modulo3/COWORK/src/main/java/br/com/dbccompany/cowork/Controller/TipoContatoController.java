package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Service.TipoContatoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/tipo-contato/")
public class TipoContatoController {

    @Autowired
    private TipoContatoService tipoContatoService;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContatoEntity novo(@RequestBody TipoContatoEntity tipo) {
        logger.info("requisicao (GET) api/tipo-contato/novo sendo processada, data: " + LocalDate.now());
        try {

            return tipoContatoService.salvar(tipo);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContatoEntity> todos() {
        logger.info("requisicao (GET) api/tipo-contato/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja contatos registrados");

        return tipoContatoService.todos();
    }

    @GetMapping(value = "buscar/por-id/{id}")
    @ResponseBody
    public TipoContatoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/tipo-contato/buscar/por-id/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    tipoContatoService.porId(id)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "buscar/por-tipo/{tipo}")
    @ResponseBody
    public TipoContatoEntity porNome(@PathVariable String tipo) {
        logger.info("requisicao (GET) api/tipo-contato/buscar/por-tipo/{tipo} sendo processada, data: " + LocalDate.now());
        logger.info("nome passado na requisicao: " + tipo);
        try {

            return Optional.ofNullable(
                    tipoContatoService.buscarPorTipo(tipo)
            ).get();

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContatoEntity editar(@PathVariable int id, @RequestBody TipoContatoEntity tipoContato) {
        logger.info("requisicao (PUT) api/tipo-contato/editar/{id} sendo processada, data: " + LocalDate.now());
        try {

            return tipoContatoService.editar(tipoContato, id);

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
