package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.Entity.UsuarioEntity;
import br.com.dbccompany.cowork.Repository.UsuarioRepository;
import br.com.dbccompany.cowork.Security.CriptografarDados;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Override
    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity salvar(UsuarioEntity entity) {

        String password = entity.getSenha();

        if(CriptografarDados.checkPassword(password)){
            entity.setSenha(new BCryptPasswordEncoder().encode(entity.getPassword()));
            return this.repository.save(entity);
        }
        return null;
    }

    public UsuarioEntity porEmail(String email) {
        return repository.findByEmail(email);
    }

    public UsuarioEntity porLogin(String login) {
        return repository.findByLogin(login).get();
    }

    @Override
    public UsuarioEntity editar(UsuarioEntity entidade, Integer id) {
        entidade.setId(id);
        repository.save(entidade);
        return entidade;
    }
}
