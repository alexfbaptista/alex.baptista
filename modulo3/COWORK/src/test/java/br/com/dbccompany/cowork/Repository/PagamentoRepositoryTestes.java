package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.*;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Entity.Enum.TipoPagamentoEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class PagamentoRepositoryTestes {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    private PagamentoEntity pagamento;

    private ContratacaoEntity contratacao;

    private ClienteEntity cliente;

    private EspacoEntity espaco;

    private PacoteEntity pacote;

    private ClientePacoteEntity clientePacote;

    @BeforeEach()
    public void setup() {

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 40, 350.0)
        );

        contratacao = contratacaoRepository.save(
                new ContratacaoEntity(cliente, espaco, TipoContratacaoEnum.DIARIA,
                        10, 0.0, 4)
        );

        pagamento = pagamentoRepository.save(
                new PagamentoEntity(contratacao, TipoPagamentoEnum.CREDITO)
        );

        pacote = pacoteRepository.save(
                new PacoteEntity(800.0)
        );

        clientePacote = clientePacoteRepository.save(
                new ClientePacoteEntity(cliente, pacote, 40)
        );
    }

    @Test
    public void retornaPagamentoProcurandoPorContratacao() {
        PagamentoEntity pagamentoBuscado = pagamentoRepository.findByContratacao(contratacao);
        assertEquals(1, pagamentoBuscado.getId());
        assertEquals(contratacao, pagamentoBuscado.getContratacao());
        assertEquals(TipoPagamentoEnum.CREDITO, pagamentoBuscado.getTipoPagamento());
    }

    @Test
    public void retornaPagamentoProcurandoPorPacote() {
        PagamentoEntity pagamentoBuscado = pagamentoRepository.findByClientePacote(clientePacote);
        assertEquals(1, pagamentoBuscado.getId());
        assertEquals(clientePacote, pagamentoBuscado.getClientePacote());
        assertEquals(TipoPagamentoEnum.CREDITO, pagamentoBuscado.getTipoPagamento());
    }
}
