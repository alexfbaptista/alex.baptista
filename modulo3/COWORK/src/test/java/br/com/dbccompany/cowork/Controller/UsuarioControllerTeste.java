package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.Entity.UsuarioEntity;
import br.com.dbccompany.cowork.Repository.UsuarioRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class UsuarioControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private UsuarioEntity usuario2;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        usuario2 = new UsuarioEntity("NomeTest", "email@email",
                "loginteste","abc123");
    }

    @Test
    public void deveRetornar200QuandoUsuario() throws Exception {
        URI uri = new URI("/api/usuario/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuario/novo");
        String json = "{\"nome\": \"Alex\", \"email\":\"alex@email.com\", \"login\":\"alexabc\", \"senha\":\"abc123\"}";

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Alex")
                )
                .andExpect(
                    MockMvcResultMatchers
                            .jsonPath("$.email")
                            .value("alex@email.com")
                )
                .andExpect(
                    MockMvcResultMatchers
                            .jsonPath("$.login")
                            .value("alexabc")
                );
    }

    @Test
    public void retornarUmUsuarioPorId() throws Exception {
        URI uri = new URI("/api/usuario/buscar/por-id/1");

        UsuarioEntity usuario = new UsuarioEntity("Alex", "alex@email.com", "alexabc", "abc123");

        repository.save(usuario);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Alex")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.email")
                                .value("alex@email.com")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.login")
                                .value("alexabc")
                );
    }

    @Test
    public void retornarUmUsuarioPorEmail() throws Exception {
        URI uri = new URI("/api/usuario/buscar/por-email/alex@email.com");

        UsuarioEntity usuario = new UsuarioEntity("Alex", "alex@email.com", "alexabc", "abc123");

        repository.save(usuario);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Alex")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.email")
                                .value("alex@email.com")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.login")
                                .value("alexabc")
                );
    }

    @Test
    public void retornarUmUsuarioPorLogin() throws Exception {
        URI uri = new URI("/api/usuario/buscar/por-login/alexabc");

        UsuarioEntity usuario = new UsuarioEntity("Alex", "alex@email.com", "alexabc", "abc123");

        repository.save(usuario);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Alex")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.email")
                                .value("alex@email.com")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.login")
                                .value("alexabc")
                );
    }

    @Test
    public void retornarUsuarioAoAtualizar() throws Exception {
        URI uri = new URI("/api/usuario/editar/1");

        repository.save(usuario2);

        usuario2.setNome("OutroNomeQualquer");

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(usuario2);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .put(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("OutroNomeQualquer")
                );
    }
}
