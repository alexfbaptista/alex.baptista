package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.CoworkingApplication;
import br.com.dbccompany.cowork.DTO.ContratacaoDTO;
import br.com.dbccompany.cowork.DTO.EditarContratacaoDTO;
import br.com.dbccompany.cowork.DTO.RespostaContratacaoDTO;
import br.com.dbccompany.cowork.Entity.ContratacaoEntity;
import br.com.dbccompany.cowork.Service.ContratacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequestMapping("api/contratacao/")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novo(@RequestBody ContratacaoDTO dto) {
        logger.info("requisicao (POST) api/contratacao/novo sendo processada, data: " + LocalDate.now());
        try {

            return "R$ " + service.salvar(dto).getValor();

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "/buscar/por-id/{id}")
    @ResponseBody
    public ContratacaoEntity porId(@PathVariable int id) {
        logger.info("requisicao (GET) api/contratacao/buscar/por-id/{id} sendo processada, data: " + LocalDate.now());
        logger.info("id passado na requisicao: " + id);
        try {

            return Optional.ofNullable(
                    service.porId(id)
            ).get();

        } catch (NoSuchElementException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @GetMapping(value = "buscar/todos")
    @ResponseBody
    public List<ContratacaoEntity> todos() {
        logger.info("requisicao (GET) api/contratacao/buscar/todos sendo processada, data: " + LocalDate.now());
        logger.warn("A lista pode voltar vazia caso nao haja contatos registrados");

        return service.todos();
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContratacaoEntity editar(@PathVariable int id, @RequestBody EditarContratacaoDTO editarContratacaoDTO) {
        logger.info("requisicao (PUT) api/contratacao/editar/{id} sendo processada, data: " + LocalDate.now());
        try {

            return service.editar(editarContratacaoDTO, id);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

}
