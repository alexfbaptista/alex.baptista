import React, { Component } from 'react';
import { EpisodiosApi } from '../../models';

import { ListaEpisodiosUi, BotaoUi, Lista, CampoBusca } from '../../components';

export default class TodosEpisodios extends Component {

  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuracao: 'ASC',
      listaEpisodios: this.props.location.state.listaEpisodios._todos.concat([])
    }
  }

  componentDidMount() {
    const { listaEpisodios } = this.props.location.state;
    const requisicoes = listaEpisodios._todos.map( e => this.episodiosApi.buscarDetalhes( e.id ) );
    Promise.all( requisicoes ).then( resultados => {
      listaEpisodios._todos.forEach( episodio => {
        episodio.dataEstreia = resultados.find( e => e.id === episodio.id ).dataEstreia
      });
      this.setState( state => { return { ...state } } )
    })
  }

  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => new Date( ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia ) - new Date( ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia ),
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    } );
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState( {
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao - ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).duracao,
      tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    } );
  }

  filtrarPorTermo = evt => {
    const termo = evt.target.value;
    this.episodiosApi.filtrarPorTermo( termo )
      .then( resultados => {
        this.setState({
          listaEpisodios: this.state.listaEpisodios.filter( e => resultados.some( r => r.episodioId === e.id ) )
        })
      } )
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  //TODO: Refatorar para os dados vir em requisicao e nao estatico.
  render() {
    const { listaEpisodios } = this.state;
    listaEpisodios.sort( this.state.ordenacao );

    return (
      <React.Fragment>
        <header className="App-header">
          <h1>Todos Episódios</h1>
          <CampoBusca atualizaValor={ this.filtrarPorTermo } placeholder="Ex.: ministro" />
          <Lista
              classeName="botoes"
              dados={ [
                  { cor: "verde", nome: "Ordenar por data de estreia", metodo: this.alterarOrdenacaoParaDataEstreia },
                  { cor: "azul", nome: "Ordenar por duração", metodo: this.alterarOrdenacaoParaDuracao }
              ] }
              funcao={ ( item, i ) => this.linha( item, i ) } />
          <ListaEpisodiosUi listaEpisodios={ listaEpisodios } />
        </header>
      </React.Fragment>
    )
  }

}