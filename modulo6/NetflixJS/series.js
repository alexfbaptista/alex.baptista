let series = new Array(
    {
      titulo: "Stranger Things",
      anoEstreia: 2036,
      diretor: ["Matt Duffer","Ross Duffer"],
      genero: ["Suspense","Ficcao Cientifica","Drama"],
      elenco: ["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],
      temporadas: 2,
      numeroEpisodios: 17,
      distribuidora: "Netflix"
    },
    {
      titulo: "Game Of Thrones",
      anoEstreia: 2011,
      diretor: ["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],
      genero: ["Fantasia","Drama"],
      elenco: ["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],
      temporadas: 7,
      numeroEpisodios: 67,
      distribuidora: "HBO"
    },
    {
      titulo: "The Walking Dead",
      anoEstreia: 2010,
      diretor: ["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],
      genero: ["Terror","Suspense","Apocalipse Zumbi"],
      elenco: ["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],
      temporadas: 9,
      numeroEpisodios: 122,
      distribuidora: "AMC"
    },
    {
      titulo: "Band of Brothers",
      anoEstreia: 20001,
      diretor: ["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],
      genero: ["Guerra"],
      elenco: ["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],
      temporadas: 1,
      numeroEpisodios: 10,
      distribuidora: "HBO"
    },
    {
      titulo: "The JS Mirror",
      anoEstreia: 2017,
      diretor: ["Lisandro","Jaime","Edgar"],
      genero: ["Terror","Caos","JavaScript"],
      elenco: ["Amanda de Carli","Alex Baptista","Gilberto Junior","Gustavo Gallarreta","Henrique Klein","Isaias Fernandes","João Vitor da Silva Silveira","Arthur Mattos","Mario Pereira","Matheus Scheffer","Tiago Almeida","Tiago Falcon Lopes"],
      temporadas: 5,
      numeroEpisodios: 40,
      distribuidora: "DBC"
    },
    {
      titulo: "Mr. Robot",
      anoEstreia: 2018,
      diretor: ["Sam Esmail"],
      genero: ["Drama","Techno Thriller","Psychological Thriller"],
      elenco: ["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],
      temporadas: 3,
      numeroEpisodios: 32,
      distribuidora: "USA Network"
    },
    {
      titulo: "Narcos",
      anoEstreia: 2015,
      diretor: ["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],
      genero: ["Documentario","Crime","Drama"],
      elenco: ["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"
      ],
      temporadas: 3,
      numeroEpisodios: 30,
      distribuidora: null
    },
    {
      titulo: "Westworld",
      anoEstreia: 2016,
      diretor: ["Athena Wickham"],
      genero: ["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],
      elenco: ["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],
      temporadas: 2,
      numeroEpisodios: 20,
      distribuidora: "HBO"
    },
    {
      titulo: "Breaking Bad",
      anoEstreia: 2008,
      diretor: ["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],
      genero: ["Acao","Suspense","Drama","Crime","Humor Negro"],
      elenco: ["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],
      temporadas: 5,
      numeroEpisodios: 62,
      distribuidora: "AMC"
    })

/* ######## EXERCÍCIO 1 ######## */

Array.prototype.invalidas = function() {
  let seriesInvalidas = [];
  function aindaNaoEstreou( serie ) {
    return serie.anoEstreia > new Date().getFullYear();
  } 
  function temPropriedadeNula( serie ) {
    return Object.values(serie).some(x => x === null );
  }
  for (let i = 0; i < this.length; i++) {
    let serie = this[i];
    if ( aindaNaoEstreou( serie ) || temPropriedadeNula( serie ) ) {
      seriesInvalidas.push( serie );
    }
  }
  return seriesInvalidas;
};

console.log(`Quantidade de séries invalidas: ${ series.invalidas().length }`);

/* ######## EXERCÍCIO 2 ######## */

Array.prototype.filtrarPorAno = function( ano ) {
  let seriesDoAnoEspecificado = [];
  for(let i = 0; i < this.length; i++) {
      if( this[i].anoEstreia >= ano ) {
        seriesDoAnoEspecificado.push( this[i] );
      }
  }
  return seriesDoAnoEspecificado;
};

console.log( `Quantidade de séries que vão lançar em 2036 ou depois: ${ series.filtrarPorAno(2036).length }` );

/* ######## EXERCÍCIO 3 ######## */

Array.prototype.procurarPorNome = function( nome ) {
  let boolean = false;
  for(let i = 0; i < this.length; i++) {
    let elenco = this[i].elenco;
    for(let j = 0; j < elenco.length; j++) {
      if (elenco[j].search( nome ) >= 0) return true;
    }
  }
  return boolean;
}

console.log(`Alex esta em algum elenco? ${ series.procurarPorNome( "Alex" ) } `);
console.log(`Marcos esta em algum elenco? ${ series.procurarPorNome( "Marcos" ) } `);


/* ######## EXERCÍCIO 4 ######## */

Array.prototype.mediaDeEpisodios = function() {
  let totalDeEpisodios = 0;
  for(let i =0; i < this.length; i++) {
    if( this[i].numeroEpisodios !== null ) {
      totalDeEpisodios += this[i].numeroEpisodios;
    }
  }
  return (Math.round(totalDeEpisodios / this.length * 100) / 100).toFixed(2);;
}

console.log(`Total de média de episódios: ${ series.mediaDeEpisodios() }`);

/* ######## EXERCÍCIO 5 ######## */

Array.prototype.totalSalarios = function( index ) {
  let salarioBigBosses = 100000.0;
  let salarioOperarios = 40000.0;
  let quantidadeDeDiretores = this[index].diretor.length;
  let quantidadeDeOperarios = this[index].elenco.length;
  return (salarioBigBosses * quantidadeDeDiretores) + (salarioOperarios * quantidadeDeOperarios);
}

console.log( `Total salario do Game Of Thrones: ${ series.totalSalarios( 1 ) }` );

/* ######## EXERCÍCIO 6 ######## */

//Exercicio A
Array.prototype.queroGenero = function( genero ) {
  let seriesEncontradas = [];
  for(let i = 0; i < this.length; i++) {
    let generosDaSerie = this[i].genero;
    for(let j = 0; j < generosDaSerie.length; j++) {
      if( generosDaSerie[j] == genero ) {
        seriesEncontradas.push ( this[i] );
      }
    }
  }
  return seriesEncontradas;
}

console.log( `Serie(s) encontradas para o genero Caos: ${ series.queroGenero( "Caos" )[0].titulo }` );

//Exercicio B
Array.prototype.queroTitulo = function( titulo ) {
  let seriesEncontradas = [];
  for(let i = 0; i < this.length; i++) {
    if( this[i].titulo.search( titulo ) >= 0) {
      seriesEncontradas.push( this[i] );
    }
  }
  return seriesEncontradas;
}

console.log( `Serie(s) encontradas com The no nome: ${ series.queroTitulo( "The" ).length }` );

/* ######## EXERCÍCIO 7 ######## */

Array.prototype.creditos = function( index ) {
  let serie = this[index];
  let diretores = serie.diretor;
  let atores = serie.elenco;
  let diretoresOrdenados = [];
  let atoresOrdenados = [];
  let creditos = "Creditos! ";
  creditos += `Titulo ${serie.titulo} | `;
  for(let i = 0; i < diretores.length; i++) {
    let arrayDoNome = diretores[i].split( " " );
    let nomeFormatado = arrayDoNome[arrayDoNome.length - 1] + ", " + arrayDoNome[0];
    diretoresOrdenados.push( nomeFormatado );
  }
  diretoresOrdenados.sort();
  creditos += "Diretores: ";
  for(let i = 0; i < diretoresOrdenados.length; i++) {
    creditos += diretoresOrdenados[i];
    if( i < diretoresOrdenados.length - 1) {
      creditos += "; ";
    } else {
      creditos += " | ";
    }
  }
  for(let i = 0; i < atores.length; i++) {
    let arrayDoNome = atores[i].split( " " );
    let nomeFormatado = arrayDoNome[arrayDoNome.length - 1] + ", " + arrayDoNome[0];
    atoresOrdenados.push( nomeFormatado );
  }
  atoresOrdenados.sort();
  creditos += "Elenco: "
  for(let i = 0; i < atoresOrdenados.length; i++) {
    creditos += atoresOrdenados[i];
    if( i < atoresOrdenados.length - 1) {
      creditos += "; ";
    }
  }
  return creditos;
}

console.log( series.creditos(0) );

/* ######## EXERCÍCIO 8 ######## */

Array.prototype.hashtagSecreta = function() {
  let elencoProcurado;
  let regex = /[.]/g;
  let hashtag = "#";
  function todosTemPonto( elenco ) {
    let todosComPonto = true;
    for(let i = 0; i < elenco.length; i++) {
      if(elenco[i].search( regex ) < 0) {
        todosComPonto = false;
      } 
    }
    return todosComPonto;
  }
  for(let i = 0; i < this.length; i++) {
    if( todosTemPonto( this[i].elenco ) ) {
      elencoProcurado = this[i].elenco;
    }
  }
  for(let i = 0; i < elencoProcurado.length; i++) {
    let posicaoDaLetraAntesDoPonto = elencoProcurado[i].indexOf(".") - 1;
    hashtag += elencoProcurado[i].charAt(posicaoDaLetraAntesDoPonto);
  }
  return hashtag;
}

console.log( series.hashtagSecreta() );


