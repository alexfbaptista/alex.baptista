import React from 'react';

const CampoBusca = ( { placeholder, atualizaValor, classe } ) =>
  <React.Fragment>
    <input className={ classe }  type="text" placeholder={ placeholder } onKeyDown={ atualizaValor } />
  </React.Fragment>

export default CampoBusca;