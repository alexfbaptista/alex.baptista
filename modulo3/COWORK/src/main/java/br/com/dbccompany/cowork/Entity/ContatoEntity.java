package br.com.dbccompany.cowork.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ContatoEntity.class)
public class ContatoEntity extends EntityAbstract<Integer> implements Serializable  {

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_CONTATO")
    private TipoContatoEntity tipoContato;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private ClienteEntity cliente;

    private String valor;

    public ContatoEntity() {
    }

    public ContatoEntity(String valor) {
        this.valor = valor;
    }

    public ContatoEntity(TipoContatoEntity tipoContato, ClienteEntity cliente,
                         String valor) {
        this.tipoContato = tipoContato;
        this.cliente = cliente;
        this.valor = valor;
    }

    public ContatoEntity(Integer id, TipoContatoEntity tipoContato,
                         ClienteEntity cliente, String valor) {
        this.id = id;
        this.tipoContato = tipoContato;
        this.cliente = cliente;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
