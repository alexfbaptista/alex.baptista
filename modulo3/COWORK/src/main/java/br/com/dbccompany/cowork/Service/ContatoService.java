package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.DTO.ContatoDTO;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.ContatoRepository;
import br.com.dbccompany.cowork.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatoService extends ServiceAbstract<ContatoRepository, ContatoEntity, Integer> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity novoContato(ContatoDTO contatoDTO) {

        return repository.save(
                new ContatoEntity(
                        tipoContatoRepository.findByNome(contatoDTO.getTipoContato()),
                        clienteRepository.findByCpf(contatoDTO.getCpfCliente()),
                        contatoDTO.getValorContato()
                )
        );
    }

    @Transactional(rollbackFor = Exception.class)
    public ContatoEntity editarContato(ContatoDTO contatoDTO, int id) {

        return repository.save(
                new ContatoEntity(
                        id,
                        tipoContatoRepository.findByNome(contatoDTO.getTipoContato()),
                        clienteRepository.findByCpf(contatoDTO.getCpfCliente()),
                        contatoDTO.getValorContato()
                )
        );
    }

    public List<ContatoEntity> todosPorCliente(String cpf) {

        return repository.findAllByCliente(
                clienteRepository.findByCpf(cpf)
        );
    }

    public List<ContatoEntity> todosPorTipoContato(String tipo) {

        return repository.findAllByTipoContato(
                tipoContatoRepository.findByNome(tipo)
        );
    }

    public ContatoEntity buscarPorValor(String valor) {
        return repository.findByValor(valor);
    }
}
