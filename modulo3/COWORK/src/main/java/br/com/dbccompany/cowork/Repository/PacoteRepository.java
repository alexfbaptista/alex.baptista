package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<PacoteEntity, Integer> {
}
