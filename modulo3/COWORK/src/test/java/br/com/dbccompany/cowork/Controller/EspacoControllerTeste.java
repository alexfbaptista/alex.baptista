package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.EspacoDTO;
import br.com.dbccompany.cowork.DTO.NomeEspacoDTO;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class EspacoControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private EspacoEntity espaco;

    private ObjectMapper mapper;

    private EspacoDTO espacoDTO;

    @BeforeEach
    public void setup() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        espaco = espacoRepository.save(
                new EspacoEntity("Sala1", 10, 150.0)
        );

        mapper = new ObjectMapper();

        espacoDTO = new EspacoDTO("Sala1", 10, "R$ 150.0");
    }

    @AfterEach
    public void limpar() {
        espacoRepository.deleteAll();
    }

    @Test
    public void deveRetornar200QuandoEspaco() throws Exception {

        URI uri = new URI("/api/espaco/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmEspaco() throws Exception {

        URI uri = new URI("/api/espaco/novo");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(espacoDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Sala1")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.qtdPessoas")
                                .value(10)
                );
    }

    @Test
    public void retornarUmEspacoPorId() throws Exception {

        URI uri = new URI("/api/espaco/buscar/por-id/1");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Sala1")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.qtdPessoas")
                                .value(10)
                );
    }

    @Test
    public void retornarUmEspacoPorNome() throws Exception {

        URI uri = new URI("/api/espaco/buscar/por-nome");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                new NomeEspacoDTO("Sala1")
        );

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.qtdPessoas")
                                .value(10)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value(150.0)
                );
    }

    @Test
    public void retornarUmEspacoPorQuantidadeIgual() throws Exception {

        URI uri = new URI("/api/espaco/buscar/por-quantidade/igual/10");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("Sala1")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value(150.0)
                );
    }

    @Test
    public void deveRetornar200QuandoProcurarEspacoPorQuantidadeIgualOuMaior() throws Exception {

        URI uri = new URI("/api/espaco/buscar/por-quantidade/igual-maior/10");


        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarEspacoAtualizado() throws Exception {

        URI uri = new URI("/api/espaco/editar/1");

        espacoDTO.setNome("SALA NOVA");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(espacoDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .put(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.nome")
                                .value("SALA NOVA")
                );
    }
}
