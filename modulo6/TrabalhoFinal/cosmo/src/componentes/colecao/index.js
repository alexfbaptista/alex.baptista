import React from 'react';
import BotaoUi from '../botaoUi';
import ListaCardProduto from '../listaCardItem'

const Colecao = ({ produtoCategoria, colecao='', mensagem='', abrir='ver mais >', produtos, classe, metodo }) =>
      <React.Fragment>
          <div className={ `row container-colecao ${ classe }` } >
              <div className='row'>
                  <div className='col col-sm-12'>
                      <h1 className='nome-colecao'> { colecao } </h1>
                  </div>
              </div>
              <div className='row'>
                  <div className='col col-sm-6'>
                      <p className='mensagem-colecao'> { mensagem } </p>
                  </div>
                  <div className='col col-sm-6'>
                        <BotaoUi 
                            classe={ 'botao-colecao' }
                            nome={ abrir } 
                        />
                  </div>
              </div>
            <ListaCardProduto metodo={ metodo } produtoCategoria={ produtoCategoria } produtos={ produtos } />
          </div>
      </React.Fragment>

export default Colecao;
