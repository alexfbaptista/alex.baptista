package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Service.GerenteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/gerente/todos
 * {{url}}/api/gerente/ver/{id}
 * {{url}}/api/gerente/novo
 * {{url}}/api/gerente/editar/{id}
 */

@Controller
@RequestMapping("api/gerente/")
public class GerenteController extends ControllerAbstract<GerenteService, GerenteEntity, Integer>{
}
