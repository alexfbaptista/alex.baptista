import React, { Component } from 'react';

import './cadastro.css';

import { BotaoUi, ListaEpisodiosUi, NetflixHeader } from '../../components';


export default class CadastroUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    this.salvarLogin = this.salvarLogin.bind(this);
    this.salvaSenha = this.salvaSenha.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  salvarLogin(event) {
    this.setState({login: event.target.value});
    console.log(this.state.value)
  }

  salvaSenha(event) {
    this.setState({senha: event.target.value})
    console.log(this.state.senha)
  }

  handleSubmit(event) {
    window.localStorage.setItem(this.state.login, this.state.senha);
    event.preventDefault();
    window.location.reload();
  }

  render() {
    return (
      <React.Fragment>
        <div className='card-login-cadastro' >
          <form onSubmit={this.handleSubmit}>
            <h1 className="titulo-cadastro-login" >Cadastro Novo Usuário</h1>
            <label>
              <input type="text" value={this.state.login} onChange={this.salvarLogin} placeholder='Login...' />
            </label>
            <label>
              <input type="text" value={this.state.senha} onChange={this.salvaSenha} placeholder='Senha...' />
            </label>
            <input className="botao login-cadastro" type="submit" value="Salvar" />
            <BotaoUi link='/login' nome="Login" />
          </form>
          
        </div>
      </React.Fragment>
    );
  }
}
