package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    UsuarioEntity findByCpf(Date cpf);
    List<UsuarioEntity> findAllByNome(String nome);
    List<UsuarioEntity> findAllByEstadoCivil(String estadoCivil);

    Optional<UsuarioEntity> findByLogin(String username);
}
