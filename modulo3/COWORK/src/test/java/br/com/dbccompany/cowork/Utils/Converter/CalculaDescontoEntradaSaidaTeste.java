package br.com.dbccompany.cowork.Utils.Converter;

import br.com.dbccompany.cowork.Utils.Datas.CalculaDescontoEntradaSaida;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculaDescontoEntradaSaidaTeste {

    private LocalDateTime dataEntrada;
    private LocalDateTime dataSaida;
    private CalculaDescontoEntradaSaida diferenca;

    @BeforeEach
    public void setup() {
        dataEntrada = LocalDateTime.now();
        dataSaida = dataEntrada.plusHours(6);
        diferenca = new CalculaDescontoEntradaSaida(dataEntrada, dataSaida);
    }

    @Test
    public void retorna2Turnos() {
        assertEquals(2, diferenca.getTurnos());
    }

    @Test
    public void retorna1Turnos() {
        dataSaida = dataEntrada.plusHours(4);
        diferenca = new CalculaDescontoEntradaSaida(dataEntrada, dataSaida);
        assertEquals(1, diferenca.getTurnos());
    }

    @Test
    public void retorna3Turnos() {
        dataSaida = dataEntrada.plusHours(11);
        diferenca = new CalculaDescontoEntradaSaida(dataEntrada, dataSaida);
        assertEquals(3, diferenca.getTurnos());
    }

    @Test
    public void retorna4ParaHoras() {
        dataSaida = dataEntrada.plusHours(4);
        diferenca = new CalculaDescontoEntradaSaida(dataEntrada, dataSaida);
        assertEquals(4, diferenca.getHours());
    }

    @Test
    public void retorna30ParaMinutos() {
        dataSaida = dataEntrada.plusMinutes(30);
        diferenca = new CalculaDescontoEntradaSaida(dataEntrada, dataSaida);
        assertEquals(30, diferenca.getMinutes());
    }
}
