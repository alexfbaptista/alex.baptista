import java.util.*;

public class AgendaContatos {

   private HashMap<String, String> contatos;

    {
        contatos = new HashMap<>();
    }

    public HashMap<String, String> getContatos() {
        return contatos;
    }

    public void adicionar(String nome, String telefone) {
        contatos.put(telefone, nome);
    }

    public String buscar(String busca) {
        return (isNumero(busca)) ? porTelefone(busca) : porNome(busca);
    }

    private boolean isNumero(String busca) {
        return busca.matches("-?\\d+(\\.\\d+)?");
    }

    private String porTelefone(String busca) {
        for (String key : contatos.keySet()) {
            if (key.equals(busca)) return contatos.get(busca);
        }
        return null;
    }

    private String porNome(String busca) {
        for (String key : contatos.keySet()) {
            if (contatos.get(key).equals(busca)) return key;
        }
        return  null;
    }

public String csv() {
        ArrayList<String> keys = ordemAZDeKeysPorNomes();
        StringBuilder retorno = new StringBuilder();
        String separadorDeLinha = System.lineSeparator();
        for (String key: keys) {
            retorno.append(contatos.get(key)).append(", ").append(key).append(separadorDeLinha);
        }
        return retorno.substring(0, retorno.length() - 1);
    }

    private ArrayList<String> ordemAZDeKeysPorNomes() {
        ArrayList<String> keys = new ArrayList<>(contatos.keySet());
        String temporario;
        for (int i = 0; i < contatos.size(); i++) {
            for (int j = i + 1; j < contatos.size(); j++) {
                if (contatos.get(keys.get(i)).compareTo(contatos.get(keys.get(j))) > 0) {
                    temporario = keys.get(j);
                    keys.set(j, keys.get(i));
                    keys.set(i, temporario);

                }
            }
        }
        return keys;
    }
}