package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;
import br.com.dbccompany.vemser.Service.MovimentacoesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** URIs
 * {{url}}/api/movimentacao/todos
 * {{url}}/api/movimentacao/ver/{id}
 * {{url}}/api/movimentacao/novo
 * {{url}}/api/movimentacaoe/editar/{id}
 */

@Controller
@RequestMapping("api/movimentacao/")
public class MovimentacaoController extends ControllerAbstract<MovimentacoesService, MovimentacaoEntity, Integer> {
}
