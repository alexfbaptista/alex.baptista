package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.ContatoDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContatoEntity;
import br.com.dbccompany.cowork.Entity.TipoContatoEntity;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.ContatoRepository;
import br.com.dbccompany.cowork.Repository.TipoContatoRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDate;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ContatoControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    private ContatoDTO contatoDTO;

    private ClienteEntity cliente;

    private ContatoEntity segundoEmailParaCliente;

    private ObjectMapper mapper;

    private String json;

    @BeforeEach
    public void setup() throws JsonProcessingException {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTeste", "45781874250", LocalDate.now())
        );

        tipoContatoRepository.save(new TipoContatoEntity("email"));

        segundoEmailParaCliente = contatoRepository.save(
                new ContatoEntity(tipoContatoRepository.findByNome("email"), cliente, "segundoemail@email.com")
        );

        contatoDTO = new ContatoDTO("email", "teste@email.com", "45781874250");

        mapper = new ObjectMapper();

        json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contatoDTO);
    }

    @Test
    public void retornarContatoPorEmail() throws Exception {

        URI uri = new URI("/api/contato/buscar/por-id/1");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value("segundoemail@email.com")
                );
    }

    @Test
    public void deveRetornar200QuandoContatoAcesso() throws Exception {

        URI uri = new URI("/api/contato/buscar/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarContatoPorTipoContato() throws Exception {

        URI uri = new URI("/api/contato/buscar/por-tipo-contato/email");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornarContatoPorCPF() throws Exception {

        URI uri = new URI("/api/contato/buscar/por-cliente/45781874250");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarContato() throws Exception {

        URI uri = new URI("/api/contato/novo");

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .post(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.id").exists()
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value("teste@email.com")
                );
    }

    @Test
    public void retornarContatoAtualizado() throws Exception {

        URI uri = new URI("/api/contato/editar/1");

        contatoDTO.setValorContato("segundoatualziado@email.com");

        json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contatoDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .put(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value("segundoatualziado@email.com")
                );
    }

    @Test
    public void retornarContatoPorValor() throws Exception {

        URI uri = new URI("/api/contato/buscar/por-valor");

        contatoDTO.setValorContato("segundoemail@email.com");

        json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contatoDTO);

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.valor")
                                .value("segundoemail@email.com")
                );
    }
}
