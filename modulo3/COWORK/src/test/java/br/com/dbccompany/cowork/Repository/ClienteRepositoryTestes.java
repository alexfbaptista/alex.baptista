package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ClienteRepositoryTestes {

    @Autowired
    private ClienteRepository repository;

    private ClienteEntity cliente;
    private LocalDate data;

    @BeforeEach
    public void setup() {
        data = LocalDate.now();
        cliente = new ClienteEntity();
        cliente.setCpf("10114460060");
        cliente.setNome("NomeTeste");
        cliente.setDataNascimento(data);
    }

    @Test
    public void buscarPorCPF() {
        repository.save(cliente);
        ClienteEntity clientePersistido = repository.findByCpf("10114460060");
        assertEquals(cliente.getNome(), clientePersistido.getNome());
        assertEquals(cliente.getCpf(), clientePersistido.getCpf());
        assertEquals(cliente.getDataNascimento(), clientePersistido.getDataNascimento());
    }

    @Test
    public void nullBuscarPorCPFNaoRegistrado() {
        ClienteEntity clientePersistido = repository.findByCpf("10114460060");
        assertNull(clientePersistido);

    }

    @Test
    public void buscarPorNome() {
        repository.save(cliente);
        ClienteEntity clientePersistido = repository.findByNome("NomeTeste");
        assertEquals(cliente.getNome(), clientePersistido.getNome());
        assertEquals(cliente.getCpf(), clientePersistido.getCpf());
        assertEquals(cliente.getDataNascimento(), clientePersistido.getDataNascimento());
    }

    @Test
    public void nullBuscarPorNomeNaoRegistrado() {
        ClienteEntity clientePersistido = repository.findByNome("NomeTeste");
        assertNull(clientePersistido);
    }
}
