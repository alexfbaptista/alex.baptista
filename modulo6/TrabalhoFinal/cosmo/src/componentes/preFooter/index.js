import React from 'react';

const PreFooterInterno = () => 
    <React.Fragment>
        <div className='row prefooter-informacoes'>
            <div className='col col-sm-3'>
                <div className='row'>
                    <h6>utilidades</h6>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>ajuda</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>como vender</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>como comprar</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>marcas</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>termos de uso</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>política de privacidade</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>trabalhe no enjoei</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>black friday</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>investidores</a>
                </div>
            </div>
            <div className='col col-sm-3'>
                <div className='row'>
                    <h6>minha conta</h6>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>minha loja</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>minhas vendas</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>minhas compras</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>enjubank</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>yeyezados</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>configurações</a>
                </div>
            </div>
            <div className='col col-sm-3'>
                <h6>marcas populares</h6>
                <div className='col col-sm-6'>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>farm</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>melissa</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>forever 21</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>nike</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>adidas</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>kipling</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>ver todas</a>
                    </div>
                </div>
                <div className='col col-sm-6'>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>zara</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>arezzo</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>schutz</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>tommy hilfiger</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>antix</a>
                    </div>
                    <div className='row'>
                        <a href='#' className='letra-cinza'>apple</a>
                    </div>
                </div>
            </div>
            <div className='col col-sm-3'>
                <div className='row'>
                    <h6>siga a gente</h6>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>facebook</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>twitter</a>
                </div>
                <div className='row'>
                    <a href='#' className='letra-cinza'>instagram</a>
                </div>
            </div>
        </div>
    </React.Fragment>

export default PreFooterInterno;