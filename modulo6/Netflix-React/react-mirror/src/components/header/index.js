import React, { Component } from 'react';

import CampoBusca from '../campoBusca';
import BotaoUi from  '../botaoUi';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';


export default class NetflixHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    limparToken() {
        window.localStorage.removeItem('token');
      }

    render() {

        const { filtrarPorTermo = '', episodioAleatorio = '' } = this.props;

        return (
            <React.Fragment>
                <header>
                    <div>
                        <div className="netflixLogo">
                            <Link to="/home" id="logo">
                                <img src="https://github.com/carlosavilae/Netflix-Clone/blob/master/img/logo.PNG?raw=true" alt="Logo Image" />
                            </Link>
                        </div>
                        <nav className="main-nav grupo-botoes-header">
                            <CampoBusca classe="campo-buscar" atualizaValor={ filtrarPorTermo } placeholder="Ex.: ministro" />
                            <BotaoUi classe={ 'botao-header-primeiro' } link={ '/avaliacoes' } nome="Avaliações"  />
                            <BotaoUi classe={ 'botao-header-segundo' } link={ "/home" } nome="Aleatorio" metodo={ episodioAleatorio } />
                            <BotaoUi classe={ 'botao-header-terceiro' } link={ "/home" } nome="Home" metodo={ episodioAleatorio } />
                            <BotaoUi classe={ 'botao-header-quarto' } link={'/login'} nome="Deslogar" metodo={ this.limparToken }  />
                        </nav>
                    </div>
                </header>
            </React.Fragment>
        )
    }
}