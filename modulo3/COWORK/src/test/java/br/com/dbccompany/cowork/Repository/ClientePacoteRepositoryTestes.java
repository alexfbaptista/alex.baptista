package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ClientePacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClientePacoteRepositoryTestes {

    @Autowired
    private ClientePacoteRepository clientePacoteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    private ClienteEntity cliente;

    private PacoteEntity pacote;

    @BeforeEach
    public void setup() {

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        pacote = pacoteRepository.save(
                new PacoteEntity(800.0)
        );

        clientePacoteRepository.save(
                new ClientePacoteEntity(cliente, pacote, 40)
        );

        clientePacoteRepository.save(
                new ClientePacoteEntity(cliente, pacote, 40)
        );

        clientePacoteRepository.save(
                new ClientePacoteEntity(cliente, pacote, 40)
        );
    }

    @Test
    public void buscarPorClienteRetornaListaDeTamanho3() {
        List<ClientePacoteEntity> clientePacoteEntities = clientePacoteRepository.findAllByCliente(cliente);
        assertEquals(3, clientePacoteEntities.size());
    }

    @Test
    public void buscarPorPacoteRetornaListaDeTamanho3() {
        List<ClientePacoteEntity> clientePacoteEntities = clientePacoteRepository.findAllByPacote(pacote);
        assertEquals(3, clientePacoteEntities.size());
    }
}
