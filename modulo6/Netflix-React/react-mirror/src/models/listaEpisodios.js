import Episodio from "./episodio";

function _sortear ( min, max ) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor( Math.random() * (max - min) ) + min;
}

export default class ListaEpisodios {
	constructor( episodiosDoServidor = [], notasDoServidor = [], detalhesEpisodiosDoServidor = [] ){
		this._todos = episodiosDoServidor.map( elem => new Episodio( elem.id, elem.nome, elem.duracao, elem.temporada, elem.ordemEpisodio, elem.thumbUrl ) );
		this.atualizarNotas( notasDoServidor, detalhesEpisodiosDoServidor );
	}

	get avaliados() {
		return this._todos.filter( e => e.nota ).sort( ( a,b ) => b.nota- a.nota );
	}

	get episodiosAleatorios() {
		const indice = _sortear( 0, this._todos.length );
		return this._todos[ indice ];
	}

	atualizarNotas( notasDoServidor, detalhesEpisodiosDoServidor ) {
		this._todos = this._todos.map( episodio => {
			const notaArr = notasDoServidor.filter( n => n.episodioId === episodio.id ).sort( ( a,b ) => b.id - a.id );
			let nota = 0;
			for(let i = 0; i < notaArr.length; i++) {
				nota += notaArr[i].nota
			}
			nota = nota / notaArr.length
			console.log(nota)
			const detalhes = detalhesEpisodiosDoServidor.filter( n => n.episodioId === episodio.id ).sort( ( a,b ) => b.id - a.id )[0];
			episodio.nota = nota;
			episodio.sinopse = ( detalhes || {} ).sinopse;
			return episodio;
		} )
	}

}