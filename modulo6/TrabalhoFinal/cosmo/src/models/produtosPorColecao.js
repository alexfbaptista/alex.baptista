
export default class ProdutosPorCategoria {
    constructor( categorias=[], produtos=[]) {
        this.categorias = categorias;
        this.produtos = produtos;
        this.produtoCategoria = [];
        this._fazerProdutoXcategoria();
    }

    _fazerProdutoXcategoria() {
      this.categorias.map( categoria => {
        this.produtoCategoria.push({
          id: categoria.id,
          nome: categoria.nome,
          subTitulo: categoria.subTitulo,
          produtos: this.produtos.filter(produto => produto.idCategoria === categoria.id)
        })
      })
    }

    get formatar() {
        return this.produtoCategoria;
    }
}