package br.com.dbccompany.cowork.Security;

import br.com.dbccompany.cowork.Entity.UsuarioEntity;
import br.com.dbccompany.cowork.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ImplementsUserDetailsService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UsuarioEntity> usuario = usuarioRepository.findByLogin(username);
        if (usuario.isPresent()) {
            return new User(
                    usuario.get().getUsername(),
                    usuario.get().getPassword(),
                    true,
                    true,
                    true,
                    true,
                    usuario.get().getAuthorities());
        }
        throw new UsernameNotFoundException(username);
    }
}
