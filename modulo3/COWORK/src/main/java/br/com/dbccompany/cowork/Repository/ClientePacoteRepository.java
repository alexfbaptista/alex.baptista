package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ClientePacoteEntity;
import br.com.dbccompany.cowork.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<ClientePacoteEntity, Integer> {
    List<ClientePacoteEntity> findAllByCliente(ClienteEntity cliente);
    List<ClientePacoteEntity> findAllByPacote(PacoteEntity pacote);
}
