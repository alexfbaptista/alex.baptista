import React, { Component } from 'react';
import MeuInputNumero from '../meuInputNumero';
import Rating from '@material-ui/lab/Rating';
import './episodioCard.css'

export default class EpisodioUi extends Component {
  constructor(props) {
		super(props)
	}

  render(){
    const { episodio, classeCard, classeImg, detalhes, deveExibirErro, registrarNota } = this.props;
    return (
      <React.Fragment>
        <div className={ classeCard } >
          <img className={ classeImg }  src={ episodio.url } alt="Avatar" />
          <div className="container">
            <h2 className="titulo-card"> { episodio.nome } </h2>
            <Rating name="half-rating-read" defaultValue={ episodio.nota } precision={0.5} readOnly />

            <p>{ episodio.sinopse }</p>
            
            { 
              detalhes &&
                <article>
                  <p>IMDb: { detalhes.notaImdb * 0.5 } - Estreia: { new Date( detalhes.dataEstreia ).toLocaleDateString() } </p>
                </article> 
            }

            <p> Temp/Epi: { episodio.temporadaEpisodio } - Duração: { episodio.duracaoEmMin } </p>

            {
              detalhes &&
              <MeuInputNumero
                className="descricao-nota"
                placeholder="Nota de 1 a 5"
                mensagem="Qual a sua nota para esse episódio?"
                obrigatorio={true}
                visivel={ true }
                deveExibirErro={deveExibirErro}
                registrarNota={ registrarNota } 
              />
            }

          </div>
        </div>
      </React.Fragment>
    )
  }
}