package br.com.dbccompany.logs.Controller;

import br.com.dbccompany.logs.Entity.Logs;
import br.com.dbccompany.logs.Service.LogsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/logs")
public class LogsController {

    private LogsService logsService;

    public LogsController(LogsService logsService) {
        this.logsService = logsService;
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Logs> todos() {
        return logsService.findAll();
    }

    @GetMapping(value = "/buscar/porid/{id}")
    @ResponseBody
    public Logs porId(@PathVariable String id) {
        return logsService.findById(id);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Logs novo(@RequestBody String data) {
        return logsService.save(data);
    }

}
