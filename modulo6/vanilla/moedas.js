let moedas = ( function () {
    //tudo escrito aqui é privado;
    function imprimirMoeda ( params ) {
        arredondar = ( numero, precisao = 2 ) => {
            const fator = Math.pow( 10, precisao);
            return Math.ceil( numero * fator ) / fator;
        }

        formatarDecimal = ( valor ) => {
            return arredondar(valor).toString().replace("0.", "").padStart(2, 0);
        }

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params;

        const parteDecimal = formatarDecimal(Math.abs(numero)%1); //02
        let parteInteira = Math.trunc(numero).toString();
        let restoDoDivisor = parteInteira.length%3; //1

        let milharInteiroFormatado = [...parteInteira].reduce( (acumulador, valorASeguir, i) => ((i - restoDoDivisor) % 3 == 0) ? acumulador.concat(`${separadorMilhar}${valorASeguir}`) : acumulador.concat(`${valorASeguir}`));
        const numeroFormatado = `${ milharInteiroFormatado }${ separadorDecimal }${ parteDecimal }`;
        
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
        
    }

    //Tudo que eu escrever no return é publico;
    return {
        imprimirBRL: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-R$ ${ numeroFormatado }`
            }),
        imprimirGBP: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-£ ${ numeroFormatado }`
            }),
        imprimirFR: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `${ numeroFormatado } €`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado } €`
            })
    }

})()

console.log(moedas.imprimirBRL(2313477.0135));
console.log(moedas.imprimirBRL(477.0135));
console.log(moedas.imprimirGBP(2313477.0135));
console.log(moedas.imprimirGBP(477.0135));
console.log(moedas.imprimirFR(2313477.0135));
console.log(moedas.imprimirFR(477.0135));





// let moedas = (function () {
//     //tudo escrito aqui é privado;

//     function imprimirMoeda(params) {

//         function arredondar(numero, precisao = 2) {
//             const fator = Math.pow(10, precisao);
//             return Math.ceil(numero * fator) / fator;
//         }

//         const {
//             numero,
//             separadorMilhar,
//             separadorDecimal,
//             colocarMoeda,
//             colocarNegativo
//         } = params;

//         const qtdCasasMilhares = 3;
//         let StringBuffer = [];
//         let parteDecimal = arredondar(Math.abs(numero) % 1);
//         let parteInteira = Math.trunc(numero);
//         let parteInteiraString = Math.abs(parteInteira).toString();
//         let tamanhoParteInteira = parteInteiraString.length;

//         //.477 .313 2
//         let contador = 1;

//         while (parteInteiraString.length > 0) {
//             if (contador % qtdCasasMilhares == 0 && parteInteiraString.length > 3) {
//                 StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(tamanhoParteInteira - contador)}`);
//                 parteInteiraString = parteInteiraString.slice(0, tamanhoParteInteira - contador);
//             } else if (parteInteiraString.length <= qtdCasasMilhares) {
//                 StringBuffer.push(parteInteiraString);
//                 parteInteiraString = '';
//             }
//             contador++;
//         }

//         StringBuffer.push(parteInteiraString);

//         let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, 0);

//         const numeroFormatado = `${StringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;

//         return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
//     }

//     //Tudo que eu escrever no return é publico;
//     return {
//         imprimirBRL: (numero) =>
//             imprimirMoeda({
//                 numero,
//                 separadorMilhar: '.',
//                 separadorDecimal: ',',
//                 colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
//                 colocarNegativo: numeroFormatado => `-R$ ${numeroFormatado}`
//             }),
//         imprimirGBP: (numero) =>
//             imprimirMoeda({
//                 numero,
//                 separadorMilhar: ',',
//                 separadorDecimal: '.',
//                 colocarMoeda: numeroFormatado => `£ ${numeroFormatado}`,
//                 colocarNegativo: numeroFormatado => `-£ ${numeroFormatado}`
//             }),
//         imprimirFR: (numero) =>
//             imprimirMoeda({
//                 numero,
//                 separadorMilhar: '.',
//                 separadorDecimal: ',',
//                 colocarMoeda: numeroFormatado => `${numeroFormatado} €`,
//                 colocarNegativo: numeroFormatado => `-${numeroFormatado} €`
//             })
//     }

// })()

// console.log(moedas.imprimirBRL(2313477.0135));
// console.log(moedas.imprimirBRL(477.0135));
// console.log(moedas.imprimirGBP(2313477.0135));
// console.log(moedas.imprimirGBP(477.0135));
// console.log(moedas.imprimirFR(2313477.0135));
// console.log(moedas.imprimirFR(477.0135));