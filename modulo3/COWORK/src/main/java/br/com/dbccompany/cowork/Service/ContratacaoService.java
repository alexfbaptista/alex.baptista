package br.com.dbccompany.cowork.Service;

import br.com.dbccompany.cowork.DTO.ContratacaoDTO;
import br.com.dbccompany.cowork.DTO.EditarContratacaoDTO;
import br.com.dbccompany.cowork.DTO.RespostaContratacaoDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.ContratacaoEntity;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EspacoService espacoService;

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Transactional( rollbackFor = Exception.class )
    public RespostaContratacaoDTO salvar(ContratacaoDTO contratacaoDTO) {

        ClienteEntity cliente = clienteService
                .buscarPorCpf(contratacaoDTO.getCpfCliente());

        EspacoEntity espaco = espacoService
                .buscarPorNome(contratacaoDTO.getEspacoNome());

        ContratacaoEntity contratacao = getContratacaoEntity(contratacaoDTO, cliente, espaco);

        contratacaoRepository.save(contratacao);

        return new RespostaContratacaoDTO(getValor(espaco, contratacao));
    }

    public ContratacaoEntity editar(EditarContratacaoDTO editarContratacaoDTO, int id) {

        Optional<ContratacaoEntity> contratacao = contratacaoRepository.findById(id);

        if (contratacao.isPresent()) {

            atualizaContratacao(editarContratacaoDTO, contratacao);

            return contratacaoRepository.save(contratacao.get());
        }

        throw new NoSuchElementException();
    }

    public ContratacaoEntity porId(int id) {
        return contratacaoRepository.findById(id).get();
    }

    public List<ContratacaoEntity> todos() {
        return (List<ContratacaoEntity>) contratacaoRepository.findAll();
    }

    public double getValor(EspacoEntity espaco, ContratacaoEntity contratacao) {
        return espaco.getValor() * contratacao.getQuantidade();
    }

    public ContratacaoEntity getContratacaoEntity(ContratacaoDTO contratacaoDTO, ClienteEntity cliente, EspacoEntity espaco) {
        ContratacaoEntity contratacao = new ContratacaoEntity(
                cliente,
                espaco,
                contratacaoDTO.getTipo(),
                contratacaoDTO.getQuantidade(),
                contratacaoDTO.getDesconto(),
                contratacaoDTO.getPrazo()
        );
        return contratacao;
    }

    public void atualizaContratacao(EditarContratacaoDTO editarContratacaoDTO, Optional<ContratacaoEntity> contratacao) {
        contratacao.get()
                .setTipoContratacao(editarContratacaoDTO.getTipoContratacao());
        contratacao.get()
                .setQuantidade(editarContratacaoDTO.getQuantidade());
        contratacao.get()
                .setDesconto(editarContratacaoDTO.getDesconto());
        contratacao.get()
                .setPrazo(editarContratacaoDTO.getPrazo());
    }
}