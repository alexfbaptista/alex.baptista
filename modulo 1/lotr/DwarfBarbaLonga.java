public class DwarfBarbaLonga extends Dwarf {

    private boolean equipado;
    private  DadoD6 d6 = new DadoD6();

    public DwarfBarbaLonga(String nome) {
        super(nome);
    }

    {
        this.equipado = false;
        this.qtdDano = 10.0;
    }

    @Override
    public void sofrerDano() {
        if (perdeVida()) {
            super.vida -= super.vida >= calcularDano() ? calcularDano() : super.vida;
            super.status = estaVivo() ? Status.SOFREU_DANO : Status.MORTO;
        }
    }

    private boolean perdeVida() {
        return d6.getDado() > 2;
    }

    protected double calcularDano() {
        return this.equipado ? 5.0 : this.qtdDano;
    }

    public void equiparEscudo() {
        this.equipado = true;
    }
}
