package br.com.dbccompany.cowork.Utils.Converter;

public class ConversorValorStringXDouble {

    private String valorFormatado;

    public ConversorValorStringXDouble(String valorFormatado) {
        this.valorFormatado = valorFormatado;
    }

    public ConversorValorStringXDouble(Double valorSemFormatacao) {
        this.valorFormatado = "R$ " + valorSemFormatacao.toString();
    }

    public String getValorFormatadoEmString() {
        return valorFormatado;
    }

    public double getValorEmDouble() {
        String [] valorEmString = valorFormatado.split(" ");
        return Double.parseDouble(valorEmString[1]);
    }
}
