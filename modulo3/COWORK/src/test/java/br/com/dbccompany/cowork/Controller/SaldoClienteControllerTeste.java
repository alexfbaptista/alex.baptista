package br.com.dbccompany.cowork.Controller;

import br.com.dbccompany.cowork.DTO.SaldoClienteDTO;
import br.com.dbccompany.cowork.Entity.ClienteEntity;
import br.com.dbccompany.cowork.Entity.EspacoEntity;
import br.com.dbccompany.cowork.Entity.SaldoClienteEntity;
import br.com.dbccompany.cowork.Entity.SaldoClienteId;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Repository.ClienteRepository;
import br.com.dbccompany.cowork.Repository.EspacoRepository;
import br.com.dbccompany.cowork.Repository.SaldoClienteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class SaldoClienteControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private SaldoClienteEntity saldoCliente;

    private ClienteEntity cliente;

    private EspacoEntity espaco;

    private SaldoClienteId saldoClienteId;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        cliente = clienteRepository.save(
                new ClienteEntity("NomeTest", "44477788822", LocalDate.now())
        );

        espaco = espacoRepository.save(
                new EspacoEntity("SalaTeste", 40, 350.0)
        );

        saldoClienteId = new SaldoClienteId(cliente.getId(), espaco.getId());

        saldoCliente = saldoClienteRepository.save(
                new SaldoClienteEntity(
                        saldoClienteId,
                        TipoContratacaoEnum.MES, 50, LocalDateTime.now()
                )
        );
    }

    @Test
    public void deveRetornar200QuandoSaldoCliente() throws Exception {

        URI uri = new URI("/api/saldo-cliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void retornaSaldoClientePorId() throws Exception {

        URI uri = new URI("/api/saldo-cliente/buscar/por-id");

        ObjectMapper mapper = new ObjectMapper();

        saldoClienteRepository.save(saldoCliente);

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                new SaldoClienteDTO("44477788822", "SalaTeste")
        );

        mockMvc
                .perform(

                        MockMvcRequestBuilders
                                .get(uri)
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.tipoContratacao")
                                .value("MES")
                )
                .andExpect(
                        MockMvcResultMatchers
                                .jsonPath("$.quantidade")
                                .value(50)
                );
    }
}
