package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.EspacoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class EspacoRepositoryTestes {

    @Autowired
    private EspacoRepository repository;

    private EspacoEntity espaco;

    @BeforeEach
    public void setup() {
        espaco = new EspacoEntity();
        espaco.setNome("Sala 01 - Reuniao Executiva");
        espaco.setQtdPessoas(20);
        espaco.setValor(450.0);
    }

    @Test
    public void buscarPorNome() {
        repository.save(espaco);
        EspacoEntity espacoPersistido = repository.findByNome("Sala 01 - Reuniao Executiva");
        assertEquals(espaco.getNome(), espacoPersistido.getNome());
        assertEquals(espaco.getQtdPessoas(), espacoPersistido.getQtdPessoas());
    }

    @Test
    public void nullBuscarPorNomeNaoRegistrado() {
        EspacoEntity espacoPersistido = repository.findByNome("Sala 01 - Reuniao Executiva");
        assertNull(espacoPersistido);
    }

    @Test
    public void buscarPorQuantidadeDePessoas() {
        repository.save(espaco);
        EspacoEntity espacoPersistido = repository.findByQtdPessoas(20);
        assertEquals(espaco.getNome(), espacoPersistido.getNome());
    }

    @Test
    public void nullBuscarPorQuantidadeDePessoasNaoRegistrado() {
        EspacoEntity espacoPersistido = repository.findByQtdPessoas(20);
        assertNull(espacoPersistido);
    }

    @Test
    public void buscarPorQuantidadeDePessoasMaiorOuIgualValorFornecido() {
        repository.save(espaco);
        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("Sala 02 - Reuniao Executiva");
        espaco2.setQtdPessoas(350);
        espaco2.setValor(1950.0);
        repository.save(espaco2);
        EspacoEntity espaco3 = new EspacoEntity();
        espaco3.setNome("Sala 03 - Reuniao Executiva");
        espaco3.setQtdPessoas(9);
        espaco3.setValor(1950.0);
        repository.save(espaco3);
        List<EspacoEntity> espacos = repository.findByQtdPessoasGreaterThanEqual(20);
        assertEquals(2, espacos.size());
    }
}
