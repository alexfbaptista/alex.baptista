import React from 'react';
import BotaoUi from '../botaoUi';

const MenuNavegacaoResponsivo = ({  }) =>
    <React.Fragment>
        <div className='row botoes-container-navegacao-responsivo primeiro-row-botao-navegacao-responsivo'>
            <div className='col col-sm-6'>
                <BotaoUi classe='botao-navegacao-responsivo letra-salmao' nome='entrar' link='/login' />
            </div>
            <div className='col col-sm-6'>
                <BotaoUi classe='botao-navegacao-responsivo botao-navegacao-responsivo-salmao' nome='quero vender' />
            </div>
        </div>
        <div className='row botoes-container-navegacao-responsivo'>
            <div className='col col-sm-6'>
                <BotaoUi classe='botao-navegacao-responsivo letra-salmao' nome='registrar' link='/cadastro' />
            </div>
            <div className='col col-sm-6'>
                <BotaoUi classe='botao-navegacao-responsivo botao-duvida' nome='' />
            </div>
        </div>
    </React.Fragment>

export default MenuNavegacaoResponsivo;