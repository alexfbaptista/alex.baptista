import React from 'react';
import CardProduto from '../cardProduto';

const ListaCardProduto = ({ produtos, produtoCategoria, metodo }) =>
      <React.Fragment>
        <div className='row'>
        {
          produtos.map( e =>
            <CardProduto metodo={ metodo } produto={ e } produtoCategoria={ produtoCategoria } />
          )
        }
        </div>
      </React.Fragment>

export default ListaCardProduto;