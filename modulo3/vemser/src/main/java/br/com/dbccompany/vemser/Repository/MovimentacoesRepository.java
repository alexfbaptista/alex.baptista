package br.com.dbccompany.vemser.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;


import java.util.List;

@Repository
public interface MovimentacoesRepository extends CrudRepository<MovimentacaoEntity, Integer> {
    List<MovimentacaoEntity> findAllByConta(int idConta);
    List<MovimentacaoEntity> findAllByTipoMovimentacao(String tipoMovimentacao);
}
