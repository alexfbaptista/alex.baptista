import React from 'react';
import ReactDOM from 'react-dom';
import App from './container/App';
import reportWebVitals from './reportWebVitals';
import 'antd/dist/antd.css';
import './estilos/index.css';
import './estilos/general.css';
import './estilos/grid.css';
import './estilos/promocao.css';
import './estilos/header.css';
import './estilos/botao.css';
import './estilos/banner.css';
import './estilos/colecao.css';
import './estilos/formulario.css';
import './estilos/paginaInterna.css';
import './estilos/letrasCores.css';
import './estilos/menu.css';
import './estilos/footer.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
