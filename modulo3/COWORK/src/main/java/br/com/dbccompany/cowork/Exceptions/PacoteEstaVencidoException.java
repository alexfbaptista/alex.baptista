package br.com.dbccompany.cowork.Exceptions;

public class PacoteEstaVencidoException extends Exception {

    private String mensagem;

    public PacoteEstaVencidoException(String mensagem) {
        super("Erro: o pacote venceu na data: " + mensagem);
        this.mensagem = mensagem;
    }
}
