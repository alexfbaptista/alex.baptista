import java.util.ArrayList;

public class AtaqueIntercalado extends EstrategiasDeAtaque {

    public AtaqueIntercalado(ExercitoDeElfos exercito) {
        super(exercito);
    }

    @Override
    void atacar(Dwarf dwarf) {
        if (composicao()) {
            for (Elfo elfo : fileiraDeAtaque()) {
                elfo.atacar(dwarf);
            }
        }
    }

    @Override
    public boolean composicao() {
        return ordensElfosVerdes().size() == ordensElfosNoturnos().size();
    }

    public ArrayList<Elfo> fileiraDeAtaque() {
        ArrayList<Elfo> retorno = new ArrayList<>();
        for (int i = 0; i < ordensElfosVerdes().size();i++) {
            retorno.add(elfosAlistados.get(ordensElfosVerdes().get(i)));
            retorno.add(elfosAlistados.get(ordensElfosNoturnos().get(i)));
        }
        return retorno;
    }
}
