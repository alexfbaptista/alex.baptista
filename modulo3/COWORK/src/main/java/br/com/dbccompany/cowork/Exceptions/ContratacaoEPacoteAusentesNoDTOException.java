package br.com.dbccompany.cowork.Exceptions;

public class ContratacaoEPacoteAusentesNoDTOException extends Exception {

    public ContratacaoEPacoteAusentesNoDTOException() {
        super("Nao foi passado no DTO informacoes se o pagamento por contratacao ou pacote");
    }
}
