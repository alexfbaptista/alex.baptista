import React, { Component } from 'react';
import '../estilos/App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './home';
import FormularioLogin from './formularioLogin';
import FormularioCadastro from './formularioCadastro';
import PaginaInterna from './interno';
import { PrivateRoute } from '../componentes/rotaPrivada';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <Router>
          <Route path='/' exact component={ Home } />
          <Route path='/login' exact component={ FormularioLogin } />
          <Route path='/cadastro' exact component={ FormularioCadastro } />
          <PrivateRoute path='/interno' exact component={ PaginaInterna } />
        </Router>
      </div>
    )
  };
}
