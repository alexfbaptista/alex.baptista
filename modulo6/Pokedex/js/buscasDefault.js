/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

let idBuscadoNaUltimaRequisicao;

function getInputValue( id ) {
  const pokeApi = new PokeApi();

  pokeApi
    .buscarEspecifico( id )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );

  renderizar = ( pokemon ) => {
    const dadosPokemon = document.getElementById( 'dadosPokemon' );
    const nome = dadosPokemon.querySelector( '.nome' );
    nome.innerHTML = pokemon.nome;

    const imgPokemon = dadosPokemon.querySelector( '.thumb' );
    imgPokemon.src = pokemon.imagem;

    const altura = dadosPokemon.querySelector( '.altura' );
    altura.innerHTML = pokemon.altura;

    const idNovo = dadosPokemon.querySelector( '.id' );
    idNovo.innerHTML = pokemon.id;

    const peso = dadosPokemon.querySelector( '.peso' );
    peso.innerHTML = pokemon.peso;

    const tipos = dadosPokemon.querySelector( '.tipos' );
    tipos.innerHTML = '';
    tipos.appendChild( pokemon.tipos );

    const tipo = dadosPokemon.querySelector( '.tipo' );
    tipo.innerHTML = 'Tipo(s): '

    const status = dadosPokemon.querySelector( '.status' );
    status.innerHTML = '';
    status.appendChild( pokemon.status );

    const stat = dadosPokemon.querySelector( '.stat' );
    stat.innerHTML = 'Estatística(s): '
  }
}
