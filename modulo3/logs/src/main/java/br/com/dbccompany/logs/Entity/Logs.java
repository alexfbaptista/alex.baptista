package br.com.dbccompany.logs.Entity;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collation = "coworkLogs")
public class Logs {

    @Id
    private String id;

    private String logText;

    public Logs() {
    }

    public Logs(@NonNull String logText) {
        this.logText = logText;
    }

    @NonNull
    public String getLogText() {
        return logText;
    }

    public void setLogText(@NonNull String logText) {
        this.logText = logText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
