import React from 'react';
import { Link } from 'react-router-dom';
import { EpisodioUi } from '../';

const ListaEpisodios = ( { listaEpisodios, classeCard = "card", classeImg = "card-img" } ) =>
  <React.Fragment>
    <div className="conjunto-cards">
      {
        listaEpisodios && listaEpisodios.map( e => 
          <li key={ e.id }>
            <Link to={{ pathname: `/episodio/${ e.id }`, state: { episodio: e } }}>
              <EpisodioUi classeCard={ classeCard } classeImg={ classeImg } episodio={ e } id={ e.id } />
            </Link>
          </li>
        )
      }
    </div>
  </React.Fragment>

export default ListaEpisodios;