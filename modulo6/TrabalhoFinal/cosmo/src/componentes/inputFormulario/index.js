import React from 'react';

const InputArea = ({ classe='formulario-input', metodo, placeholder='', nome='', valor }) => (
    <React.Fragment>
        <label>
            <p className='formulario-textos-input' >{ nome }</p>
            <input className={ classe } type="text" placeholder={ placeholder } value={ valor } onChange={ metodo } />
        </label>
    </React.Fragment>
)

export default InputArea;