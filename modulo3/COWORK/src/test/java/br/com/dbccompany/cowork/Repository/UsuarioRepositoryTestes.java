package br.com.dbccompany.cowork.Repository;

import br.com.dbccompany.cowork.Entity.UsuarioEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


@DataJpaTest
public class UsuarioRepositoryTestes {

    @Autowired
    private UsuarioRepository repository;

    private UsuarioEntity usuario;

    @BeforeEach
    public void setup() {
        usuario = new UsuarioEntity(
                "NomeTeste",
                "emailTeste@email",
                "LoginTest",
                "senhaTeste123abc"
        );
    }


    @Test
    public void buscarPorLogin() {
        repository.save(usuario);
        Optional<UsuarioEntity> usuarioPersistido = repository.findByLogin("LoginTest");
        assertEquals(usuario.getEmail(), usuarioPersistido.get().getEmail());
    }

    @Test
    public void nullBuscaPorLoginNaoRegistrado() {
        Optional<UsuarioEntity> usuarioPersistido = repository.findByLogin("LoginTest");
        assertEquals(Optional.empty(), usuarioPersistido);
    }

    @Test
    public void buscarPorEmail() {
        repository.save(usuario);
        UsuarioEntity usuarioPersistido = repository.findByEmail("emailTeste@email");
        assertEquals(usuario.getEmail(), usuarioPersistido.getEmail());
    }

    @Test
    public void nullBuscarPorEmailNaoRegistrado() {
        UsuarioEntity usuarioPersistido = repository.findByEmail("emailTeste@email");
        assertNull(usuarioPersistido);
    }
}
