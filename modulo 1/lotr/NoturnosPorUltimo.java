import java.util.ArrayList;

public class NoturnosPorUltimo extends EstrategiasDeAtaque {

    public NoturnosPorUltimo(ExercitoDeElfos exercito) {
        super(exercito);
    }

    @Override
    public void atacar(Dwarf dwarf) {
        for (Elfo elfo : pelotaoOrdenado()) {
            elfo.atacar(dwarf);
        }
    }


    boolean composicao() {
        return false;
    }

    public ArrayList<Elfo> pelotaoOrdenado() {
        ArrayList<Elfo> pelotaoApto = new ArrayList<>();
        for (int i = 0; i < ordensElfosVerdes().size(); i++) {
            pelotaoApto.add(elfosAlistados.get(ordensElfosVerdes().get(i)));
        }
        for (int i = 0; i < ordensElfosNoturnos().size(); i++) {
            pelotaoApto.add(elfosAlistados.get(ordensElfosNoturnos().get(i)));
        }
        return pelotaoApto;
    }
}