package br.com.dbccompany.cowork.DTO;

import br.com.dbccompany.cowork.Entity.ContratacaoEntity;
import br.com.dbccompany.cowork.Entity.Enum.TipoContratacaoEnum;
import br.com.dbccompany.cowork.Entity.Enum.TipoPagamentoEnum;
import com.sun.istack.NotNull;

public class ContratacaoPacoteDTO {

    //Tabela Contratacao
    @NotNull
    private Integer idEspacos;
    @NotNull
    private Integer idCliente;
    @NotNull
    private TipoContratacaoEnum tipoContratacao;
    @NotNull
    private int quantidade;
    @NotNull
    private int prazo;
    @NotNull
    private int desconto;

    private TipoPagamentoEnum tipoPagamento;

    public ContratacaoPacoteDTO(Integer idEspacos, Integer idCliente,
                                TipoContratacaoEnum tipoContratacao, int quantidade,
                                int prazo, int desconto, TipoPagamentoEnum tipoPagamento) {
        this.idEspacos = idEspacos;
        this.idCliente = idCliente;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.prazo = prazo;
        this.desconto = desconto;
        this.tipoPagamento = tipoPagamento;
    }

    public ContratacaoEntity converterContratacao() {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(tipoContratacao);
        contratacao.setQuantidade(quantidade);
        contratacao.setPrazo(prazo);
        contratacao.setDesconto(desconto);
        return contratacao;
    }

    public Integer getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(Integer idEspacos) {
        this.idEspacos = idEspacos;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


}
