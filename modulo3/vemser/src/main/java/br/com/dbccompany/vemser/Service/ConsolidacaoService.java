package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Repository.ConsolidacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConsolidacaoService extends ServiceAbstract<ConsolidacaoRepository, ConsolidacaoEntity, Integer> {

    public ConsolidacaoEntity buscarPorAgencia(int pkAgencia) {
        return repository.findByAgencia(pkAgencia);
    }
}
